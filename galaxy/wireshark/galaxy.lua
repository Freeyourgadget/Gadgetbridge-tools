print("hello Galaxy protocol")
galaxy_proto = Proto("Galaxy",  "Galaxy Buds Protocol")

-- sent packet
-- 0 START
-- 1 SIZE (3,4,5)
-- 2 000
-- 3 COMMAND
-- 4 PARAM (if size 4)
-- 5 VALUE (if size 5)
-- 6 CRC
-- 7 CRC
-- 8 END

galaxy_proto.fields.message_pre = ProtoField.uint8("galaxy.message_pre", "Message START", base.HEX)
galaxy_proto.fields.message_end = ProtoField.uint8("galaxy.message_end", "Message END", base.HEX)
galaxy_proto.fields.message_type = ProtoField.uint8("galaxy.message_type", "Message Type", base.HEX)
galaxy_proto.fields.message_id = ProtoField.uint8("galaxy.message_id", "Message ID", base.HEX)
galaxy_proto.fields.message_param = ProtoField.uint8("galaxy.message_param", "Message Param", base.HEX)
galaxy_proto.fields.message_value = ProtoField.uint8("galaxy.message_value", "Message Value", base.HEX)
galaxy_proto.fields.message_checksum = ProtoField.uint8("galaxy.message_checksum", "Message Checksum", base.HEX)
galaxy_proto.fields.payload_length = ProtoField.int32("galaxy.payload_length", "Payload Length", base.DEC)
galaxy_proto.fields.message_payload = ProtoField.bytes("galaxy.message_payload", "Payload", base.COLON)

galaxy_proto.fields = {
    message_pre,
    message_end,
    message_type,
    message_id,
    message_param,
    message_value,
    message_checksum,
    message_payload,
    payload_length,
    message_payload
}

message_types = {
    ["00"] = "Send to buds",
    ["01"] = "Received from buds",
}

function get_message_type(message_type_code)
    local message_type = "Unknown"

    if message_types[message_type_code] ~= nil then
        message_type = message_types[message_type_code]
    end

    return message_type
end

message_ids = {
    ["80"] = "set ambient mode",
    ["84"] = "set ambient volume",
    ["85"] = "set ambient voice focus",
    ["86"] = "set equalizer",
    ["87"] = "set game mode",
    ["90"] = "set lock touch",
    ["92"] = "set touch options",
    ["a0"] = "set find start",
    ["a1"] = "set find stop",
    ["a2"] = "set find individual buds",
}

message_ids_live = {
    ["85"] = "set game mode",
    ["98"] = "set ANC",
    ["9f"] = "set pressure relief",
}

function get_message_id(message_type_code, ids)
    print(message_type_code)
    local payload_type = "Unknown"

    if ids[message_type_code] ~= nil then
        payload_type = ids[message_type_code]
    end

    return payload_type
end

function merge(t1, t2)
    -- print("merge")
    for k,v in pairs(t2) do
        -- print("me")
        -- print(v)
        t1[k]=v
    end
    return t1
end


function galaxy_proto.dissector(buffer, pinfo, tree)
    pinfo.cols.protocol = galaxy_proto.name

    local subtree = tree:add(galaxy_proto, buffer, "Galaxy Protocol Data")
    local galaxy_type = tostring(buffer(0, 1))
    local payload_len
    local ids

    subtree:add(galaxy_proto.fields.message_pre, buffer(0, 1)):append_text(" (" .. tostring(buffer(0,1)) .. ")")
    if galaxy_type == "fd" then
        print("buds live")
        print(message_ids)

        ids=merge(message_ids, message_ids_live)

        payload_len=buffer(1,1):uint()
        subtree:add(galaxy_proto.fields.payload_length, buffer(1, 1), tostring(buffer(1,1)))
        subtree:add(galaxy_proto.fields.message_type, buffer(2, 1)):append_text(" (" .. get_message_type(tostring(buffer(2,1))) .. ")")
    else
        print("buds original")
        ids=message_ids
        payload_len=buffer(2,1):uint()
        subtree:add(galaxy_proto.fields.message_type, buffer(1, 1)):append_text(" (" .. get_message_type(tostring(buffer(1,1))) .. ")")
        subtree:add(galaxy_proto.fields.payload_length, buffer(2, 1), tostring(buffer(2,1)))
    end

    subtree:add(galaxy_proto.fields.message_id, buffer(3, 1)):append_text(" (" .. get_message_id(tostring(buffer(3,1)),ids) .. ")")
    if payload_len>3 then
        subtree:add(galaxy_proto.fields.message_param, buffer(4, 1))
    end

    if payload_len>4 then
        subtree:add(galaxy_proto.fields.message_value, buffer(5, 1))
    end

    subtree:add(galaxy_proto.fields.message_payload, buffer(3, payload_len-2))
    subtree:add(galaxy_proto.fields.message_checksum, buffer(buffer:len() - 3, 2))
    subtree:add(galaxy_proto.fields.message_end, buffer(buffer:len()-1, 1)):append_text(" (" .. tostring(buffer(buffer:len()-1, 1)) .. ")")
end

local btatt_handle = DissectorTable.get("btrfcomm.dlci")
btatt_handle:add(0x04, galaxy_proto) -- Galaxy Buds OG
btatt_handle:add(0x03, galaxy_proto) -- Galaxy Buds Live

