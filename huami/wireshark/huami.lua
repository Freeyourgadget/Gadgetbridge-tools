-- Copyright (C) 2022 José Rebelo
--
-- This file is part of Gadgetbridge-tools.
--
-- Gadgetbridge is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published
-- by the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- Gadgetbridge is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

mi_proto = Proto("Huami",  "Huami Protocol")

mi_proto.fields.endpoint = ProtoField.uint8("mi.endpoint", "Endpoint", base.HEX)

local frame_number = Field.new("frame.number")
local btatt_handle = Field.new("btatt.handle")
local btatt_opcode = Field.new("btatt.opcode")

--
-- Utils
--

function on_error(subtree, message)
    subtree:add_expert_info(PI_DEBUG, PI_ERROR, message)
end

function on_warn(subtree, message)
    subtree:add_expert_info(PI_DEBUG, PI_WARN, message)
end

function assert_length(subtree, payload, length)
    if payload:len() ~= length then
        on_error(
            subtree,
            string.format(
                "Unexpected payload length: %d (expected %d)",
                payload:len(),
                length
            )
        )
    end
end

function assert_value(subtree, payload, idx, value)
    if payload(idx, 1):uint() ~= value then
        on_error(
            subtree,
            string.format(
                "Expected 0x%x at index %d, got 0x%x",
                value,
                idx,
                payload(idx, 1):uint()
            )
        )
    end
end

function assert_zero(subtree, payload, idx)
    assert_value(subtree, payload, idx, 0)
end

function to_bool(num, subtree)
    if num == 0 then
        return "false"
    elseif num == 1 then
        return "true"
    else
        on_error(subtree, string.format("Unknown value 0x%x for Boolean", num))
        return string.format("unknown (%x)", num)
    end
end

function parse_handle_noop(payload, pinfo, subtree)
    on_warn(subtree, "no-op")
end

function parse_raw_bytes_timestamp(payload, subtree, offset)
    local year = payload(offset + 0, 2):le_int()
    local month = payload(offset + 2, 1):uint()
    local day = payload(offset + 3, 1):uint()
    local hour = payload(offset + 4, 1):uint()
    local minute = payload(offset + 5, 1):uint()
    local second = payload(offset + 6, 1):uint()
    local tz = (payload(offset + 7, 1):le_int() * 15) / 60
    return string.format("%d/%02d/%02d %02d:%02d:%02d +%d", year, month, day, hour, minute, second, tz)
end

function table_length(t)
    local count = 0
    for _ in pairs(t) do
        count = count + 1
    end
    return count
end

function get_string(payload, startIdx)
    for i = startIdx, payload:len() - 1 do
        if payload(i, 1):uint() == 0 then
            return payload(startIdx, i - startIdx):string()
        end
    end

    return nil
end

--
-- Activity Data
--

activity_fetch_types = {
    [0x01] = "Activity",
    [0x05] = "Sports Summaries",
    [0x06] = "Sports Details",
    [0x07] = "Debug Logs",
    [0x12] = "Stress (Manual)",
    [0x13] = "Stress (Auto)",
}

-- Keep a map from (start_frame_number) -> (end_frame_number, type)
local fetch_packets = {}

function find_fetch_info(frame_num)
    -- Given a frame number, find the closest previous frame in fetch_packets

    min_frame = -1

    for i, finfo in pairs(fetch_packets) do
        if frame_num > i and i > min_frame and (fetch_packets[i]['end'] == nil or fetch_packets[i]['end'] > i) then
            min_frame = i
        end
    end

    if min_frame == -1 then
        return nil
    end

    return fetch_packets[min_frame]
end

function parse_fetch(payload, pinfo, subtree)
    if payload(0, 1):uint() == 0x01 then
        pinfo.cols.info = "Activity Fetch - Request"
        assert_length(subtree, payload, 10)

        fetch_type = activity_fetch_types[payload(1, 1):uint()]
        fetch_ts = parse_raw_bytes_timestamp(payload, subtree, 2)

        if fetch_type == nil then
            fetch_type = string.format("Unknown (0x%x)", payload(1, 1):uint())
            on_warn(subtree, string.format("Unknown activity fetch type 0x%x ", payload(1, 1):uint()))
        end

        pinfo.cols.info:append(string.format(" (%s)", fetch_type))

        if fetch_packets[frame_number().value] == nil then
            fetch_packets[frame_number().value] = {
                ['start'] = frame_number().value,
                ['type'] = payload(1, 1):uint()
            }
        end

        subtree:add(payload(1, 1), "Fetch type: " .. fetch_type)
        subtree:add(payload(2, 8), "Timestamp: " .. fetch_ts)
    elseif payload(0, 1):uint() == 0x10 then
        if payload(1, 1):uint() == 0x01 then
            pinfo.cols.info = "Activity Fetch - Request ACK"
            assert_value(subtree, payload, 1, 1)
            assert_value(subtree, payload, 2, 1)
            assert_length(subtree, payload, 15)

            subtree:add(payload(3, 4), "Number of packets: " .. payload(3, 4):le_int())
            subtree:add(payload(7, 8), "Timestamp: " .. parse_raw_bytes_timestamp(payload, subtree, 7))
        elseif payload(1, 1):uint() == 0x02 then
            pinfo.cols.info = "Activity Fetch - Start ACK"
            assert_length(subtree, payload, 3)
            subtree:add(payload(2, 1), "Success: " .. to_bool(payload(2, 1):uint(), subtree))
        elseif payload(1, 1):uint() == 0x03 then
            pinfo.cols.info = "Activity Fetch - End ACK"
            assert_length(subtree, payload, 3)
            subtree:add(payload(2, 1), "Success: " .. to_bool(payload(2, 1):uint(), subtree))
        else
            on_error(subtree, "Unexpected ack " .. payload(1, 1):uint())
        end
    elseif payload(0, 1):uint() == 0x02 then
        pinfo.cols.info = "Activity Fetch - Start"
        assert_length(subtree, payload, 1)
    elseif payload(0, 1):uint() == 0x03 then
        pinfo.cols.info = "Activity Fetch - End"
        assert_length(subtree, payload, 1)

        find_fetch_info(frame_number().value)['end'] = frame_number().value
    else
        on_error(subtree, string.format("Unexpected fetch code 0x%x", payload(0, 1):uint()))
    end
end

stress_levels = {
    [0] = "Relaxed",
    [40] = "Mild",
    [60] = "Moderate",
    [80] = "High",
}

function get_stress_desc(stress)
    max_stress = -1
    for level_min, level_desc in pairs(stress_levels) do
        if stress > level_min and stress > max_stress then
            max_stress = level_min
        end
    end

    if max_stress ~= -1 then
        return stress_levels[max_stress]
    else
        return "Unknown"
    end
end

function parse_activity_stress_manual(payload, pinfo, subtree)
    pinfo.cols.info = "Activity Data - Stress (Manual)"

    if math.fmod(payload:len() - 1, 5) ~= 0 then
        on_error(subtree, string.format("Expect data to be divisibly by 5 (it's %d) (we don't handle chunks yet)", payload:len() - 1))
        return
    end

    local count = payload(0, 1):le_int()
    subtree:add(payload(0, 1), "Count: " .. count)

    for i = 0, ((payload:len() - 1) / 5) - 1 do
        local timestamp = payload(5 * i + 1, 4):le_int()
        local stress = payload(5 * i + 5, 1):le_int()

        local stress_level = get_stress_desc(stress)

        subtree:add(payload(5 * i + 1, 5), string.format("%s - Stress: %d (%s)",  os.date("%Y/%m/%d %H:%M:%S", timestamp), stress, stress_level))
    end
end

function parse_activity_stress_auto(payload, pinfo, subtree)
    pinfo.cols.info = "Activity Data - Stress (Auto)"

    fetch_start = find_fetch_info(frame_number().value)['start']

    for i = 1, payload:len() - 1 do
        stress = payload(i, 1):uint()
        if stress ~= 255 then
            local stress_level = get_stress_desc(stress)

            subtree:add(payload(i, 1), string.format("Stress: %d (%s)", stress, stress_level))
            -- TODO: Auto stress timestamp
        end
    end
end

function parse_activity_activity(payload, pinfo, subtree)
    pinfo.cols.info = "Activity Data - Activity"

    local sample_size = 8

    fetch_start = find_fetch_info(frame_number().value)['start']

    for i = 1, payload:len() - 1, sample_size do
        local subtree_activity = subtree:add(payload(i, sample_size), "Minute data: " .. i)
        -- TODO: Activity timestamp

        local category = payload(i, 1):uint()
        local intensity = payload(i + 1, 1):uint()
        local steps = payload(i + 2, 1):uint()
        local heartrate = payload(i + 3, 1):uint()

        local unknown1 = payload(i + 4, 1):uint()
        local unknown2 = payload(i + 5, 1):uint()
        local unknown3 = payload(i + 6, 1):uint()
        local unknown4 = payload(i + 7, 1):uint()

        subtree:add(payload(i, 1), string.format("Category: 0x%x", category))
        subtree:add(payload(i, 1), string.format("Intensity: %x", intensity))
        subtree:add(payload(i, 1), string.format("Steps: %x", steps))
        subtree:add(payload(i, 1), string.format("Heartrate: %d", heartrate))
        subtree:add(payload(i, 1), string.format("Unknown1: 0x%x", unknown1))
        subtree:add(payload(i, 1), string.format("Unknown2: 0x%x", unknown2))
        subtree:add(payload(i, 1), string.format("Unknown3: 0x%x", unknown3))
        subtree:add(payload(i, 1), string.format("Unknown4: 0x%x", unknown4))
    end
end

function parse_activity(payload, pinfo, subtree)
    fetch_packet = find_fetch_info(frame_number().value)

    if fetch_packet == nil then
        on_error(subtree, string.format("Failed to find corresponding fetch packet start"))
        return
    end

    activity_type = fetch_packet['type']
    activity_type_name = activity_fetch_types[activity_type]

    if activity_type == 0x01 then
        parse_activity_activity(payload, pinfo, subtree)
    elseif activity_type == 0x12 then
        parse_activity_stress_manual(payload, pinfo, subtree)
    elseif activity_type == 0x13 then
        parse_activity_stress_auto(payload, pinfo, subtree)
    elseif activity_type_name ~= nil then
        pinfo.cols.info = string.format("Activity Data - %s", activity_type_name)
        -- TODO: Parse
    else
        pinfo.cols.info = string.format("Activity Data - Unknown (0x%x)", activity_type)
        on_warn(subtree, string.format("Unknown activity fetch type 0x%x", activity_type))
    end
end

--
-- Heart Rate Monitoring
--

function parse_heart_rate_alert(payload, subtree)
    assert_length(subtree, payload, 5)
    assert_zero(subtree, payload, 2)
    subtree:add(payload(3, 1), "Heart Rate Alert: " .. to_bool(payload(3, 1):uint(), subtree))
    -- 100 to 150, steps of 5
    subtree:add(payload(4, 1), "Threshold: " .. payload(4, 1):uint() .. "bpm")
end

function parse_handle_hr_measurement(payload, pinfo, subtree)
    assert_length(subtree, payload, 2)
    local val = payload(1, 1):uint()
    pinfo.cols.info = "Heart Rate Measurement"
    subtree:add(payload(1, 1), string.format("Value: %d bpm", val))
end

--
-- Configuration
--

function parse_request_config(payload, pinfo, subtree)
    assert_length(subtree, payload, 5)
    assert_zero(subtree, payload, 2)
    assert_zero(subtree, payload, 3)
    assert_zero(subtree, payload, 4)

    config = payload(1, 1):uint()
    if config == 0x01 then
        pinfo.cols.info:append(", Alarms with times")
    elseif config == 0x11 then
        pinfo.cols.info:append(", Workout Screen Activity Types")
    else
        on_warn(subtree, string.format("Unexpected config request 0x%x", config))
    end
end

function parse_restrict_pairing(payload, subtree)
    assert_length(subtree, payload, 4)
    assert_zero(subtree, payload, 2)

    subtree:add(payload(3, 1), "Restrict pairing: " .. to_bool(payload(3, 1):uint(), subtree))
end

function parse_screen_unlock(payload, subtree)
    assert_length(subtree, payload, 4)
    assert_zero(subtree, payload, 2)

    subtree:add(payload(3, 1), "Screen Unlock: " .. to_bool(payload(3, 1):uint(), subtree))
end

function parse_time_format(payload, subtree)
    assert_length(subtree, payload, 4)
    assert_zero(subtree, payload, 2)

    config = payload(3, 1):uint()
    if config == 0x00 then
        subtree:add(payload(3, 1), "Format: 12H")
    elseif config == 0x01 then
        subtree:add(payload(3, 1), "Format: 24H")
    else
        on_warn(subtree, string.format("Unexpected time format 0x%x", config))
    end
end

function parse_date_format(payload, subtree)
    if (payload:len() == 14) then
        -- Date is null terminated (eg. Mi Band 3)
        assert_zero(subtree, payload, payload:len() - 1)
    else
        assert_length(subtree, payload, 13)
    end
    assert_zero(subtree, payload, 2)

    subtree:add(payload(3, 10), "Date Format: " .. payload(3, 10):string())
end

function parse_date_display(payload, subtree)
    assert_length(subtree, payload, 4)
    assert_zero(subtree, payload, 2)

    config = payload(3, 1):uint()
    if config == 0x03 then
        subtree:add(payload(3, 1), "Format: Date/Time")
    elseif config == 0x00 then
        subtree:add(payload(3, 1), "Format: Time")
    else
        on_warn(subtree, string.format("Unexpected date format 0x%x", config))
    end
end

function parse_language(payload, subtree)
    assert_length(subtree, payload, 8)
    assert_zero(subtree, payload, 2)

    local lang = payload(3, payload:len() - 3):string()

    subtree:add(payload(3, lang:len()), "Language: " .. lang)
end

display_items_old = {
    -- 0?
    [1] = "notifications",
    [2] = "weather",
    [3] = "activity",
    [4] = "more",
    [5] = "status",
    [6] = "heart_rate",
    [7] = "timer",
    [8] = "nfc",
    -- 12?
    -- 13?
}

function parse_display_items_old(payload, pinfo, subtree)
    local enabled_mask = payload(1, 2):le_int();
    local enabled_items = ""

    for i = 0, 16 do
        if bit32.band(enabled_mask, 2^i) > 0 then
            local display_item = display_items_old[i]

            if display_item ~= nil then
                enabled_items = enabled_items .. " " .. display_item
            end
        end
    end

    -- subtree:add(payload(1, 2), "Enabled items:" .. enabled_items)

    for i = 4, payload:len() - 1 do
        idx = payload(i, 1):uint()
        local state_str = " (unknown)"
        if bit32.band(enabled_mask, 2^idx) > 0 then
            state_str = " (enabled)"
        else
            state_str = " (disabled)"
        end

        local idx = payload(i, 1):uint()

        subtree:add(payload(i, 1), display_items_old[idx] .. state_str)

        i = i + 4
    end
end

function parse_night_mode(payload, pinfo, subtree)
    if payload(1, 1):uint() == 0 then
        assert_length(subtree, payload, 2)
        subtree:add(payload(1, 1), "Night Mode = off")
    elseif payload(1, 1):uint() == 2 then
        assert_length(subtree, payload, 2)
        subtree:add(payload(1, 1), "Night Mode = after sunset")
    elseif payload(1, 1):uint() == 1 then
        assert_length(subtree, payload, 6)
        subtree:add(payload(1, 1), "Night Mode = scheduled")
        subtree:add(payload(2, 2), string.format("Start: %02d:%02d", payload(2, 1):uint(), payload(3, 1):uint()))
        subtree:add(payload(4, 2), string.format("End: %02d:%02d", payload(4, 1):uint(), payload(5, 1):uint()))
    else
        subtree:add(payload(1, 1), "Night Mode = unknown")
        on_warn(subtree, string.format("Unexpected night mode mode 0x%x", payload(1, 1):uint()))
    end
end

function parse_do_not_disturb(payload, pinfo, subtree)
    local mode = bit32.band(payload(1, 1):uint(), bit32.bnot(0x80))
    local lift_wrist = bit32.band(payload(1, 1):uint(), 0x80) > 0

    if mode == 0x02 then
        assert_length(subtree, payload, 2)
        subtree:add(payload(1, 1), "Do Not Disturb = off")
    elseif mode == 0x03 then
        assert_length(subtree, payload, 2)
        subtree:add(payload(1, 1), "Do Not Disturb = automatic")
    elseif mode == 0x01 then
        assert_length(subtree, payload, 6)
        subtree:add(payload(1, 1), "Do Not Disturb = scheduled")
        subtree:add(payload(2, 2), string.format("Start: %02d:%02d", payload(2, 1):uint(), payload(3, 1):uint()))
        subtree:add(payload(4, 2), string.format("End: %02d:%02d", payload(4, 1):uint(), payload(5, 1):uint()))
    else
        subtree:add(payload(1, 1), "Do Not Disturb = unknown")
        on_warn(subtree, string.format("Unexpected Do Not Disturb mode 0x%x", payload(1, 1):uint()))
    end

    subtree:add(payload(1, 1), string.format("Lift Wrist while DND = %s", lift_wrist))
end

function parse_inactivity_warnings(payload, pinfo, subtree)
    assert_length(subtree, payload, 12)

    subtree:add(payload(1, 1), "Inactivity Warnings: " .. to_bool(payload(1, 1):uint(), subtree))

    local schd_start_h = payload(4, 1):uint()
    local schd_start_m = payload(5, 1):uint()
    local schd_end_h = payload(6, 1):uint()
    local schd_end_m = payload(7, 1):uint()
    subtree:add(payload(4, 4), string.format("Interval 1: %02d:%02d to %02d:%02d ", schd_start_h, schd_start_m, schd_end_h, schd_end_m))

    local schd_start_h = payload(8, 1):uint()
    local schd_start_m = payload(9, 1):uint()
    local schd_end_h = payload(10, 1):uint()
    local schd_end_m = payload(11, 1):uint()
    subtree:add(payload(8, 4), string.format("Interval 2: %02d:%02d to %02d:%02d ", schd_start_h, schd_start_m, schd_end_h, schd_end_m))
end

function parse_lift_wrist(payload, subtree)
    assert_length(subtree, payload, 8)
    assert_zero(subtree, payload, 2)

    if payload(3, 1):uint() == 0 then
        subtree:add(payload(3, 1), "Lift On Wrist: off")
        for i = 4, 7 do assert_zero(subtree, payload, i) end
    elseif payload(3, 1):uint() == 1 then
        if payload(4, 4):uint() == 0 then
            subtree:add(payload(3, 1), "Lift On Wrist: on")
            for i = 4, 7 do assert_zero(subtree, payload, i) end
        else
            subtree:add(payload(3, 1), "Lift On Wrist: scheduled")
            subtree:add(payload(4, 2), string.format("Start: %02d:%02d", payload(4, 1):uint(), payload(5, 1):uint()))
            subtree:add(payload(6, 2), string.format("End: %02d:%02d", payload(6, 1):uint(), payload(7, 1):uint()))
        end
    else
        subtree:add(payload(3, 1), "Lift On Wrist: unknown")
        on_warn(subtree, "Unexpected lift wrist " .. payload(3, 1):uint())
    end
end

function parse_lift_wrist_response_speed(payload, subtree)
    assert_length(subtree, payload, 4)
    assert_zero(subtree, payload, 2)

    if payload(3, 1):uint() == 0 then
        subtree:add(payload(3, 1), "Lift On Wrist Response: normal")
    elseif payload(3, 1):uint() == 1 then
        subtree:add(payload(3, 1), "Lift On Wrist Response: sensitive")
    else
        subtree:add(payload(3, 1), "Lift On Wrist Response: unknown")
        on_warn(subtree, "Unexpected lift wrist response speed")
    end
end

function parse_activity_monitoring(payload, subtree)
    assert_length(subtree, payload, 4)
    assert_zero(subtree, payload, 2)
    subtree:add(payload(3, 1), "Activity Monitoring Enabled: " .. to_bool(payload(3, 1):uint(), subtree))
end

function parse_phone_silent_mode(payload, subtree)
    assert_length(subtree, payload, 4)
    assert_zero(subtree, payload, 2)
    subtree:add(payload(3, 1), "Silent Mode: " .. to_bool(payload(3, 1):uint(), subtree))
end

function parse_password(payload, subtree)
    assert_length(subtree, payload, 11)
    assert_zero(subtree, payload, 2)
    assert_zero(subtree, payload, payload:len() - 1)
    subtree:add(payload(3, 1), "Enabled: " .. to_bool(payload(3, 1):uint(), subtree))
    subtree:add(payload(3, 1), "Password: " .. payload(4, payload:len() - 4):string())

    -- TODO: Not tested
end

function parse_bt_connected_advertise(payload, subtree)
    assert_length(subtree, payload, 4)
    assert_zero(subtree, payload, 2)
    subtree:add(payload(3, 1), "Enabled: " .. to_bool(payload(3, 1):uint(), subtree))
end

function parse_distance_unit(payload, subtree)
    assert_length(subtree, payload, 4)
    assert_zero(subtree, payload, 2)

    if payload(3, 1):uint() == 0 then
        subtree:add(payload(3, 1), "Distance Unit: metric")
    elseif payload(3, 1):uint() == 1 then
        subtree:add(payload(3, 1), "Distance Unit: imperial")
    else
        subtree:add(payload(3, 1), "Distance Unit: unknown")
        on_warn(subtree, string.format("Unexpected distance unit", payload(3, 1):uint()))
    end
end

function parse_disconnect_notification(payload, subtree)
    assert_length(subtree, payload, 8)
    assert_zero(subtree, payload, 2)
    subtree:add(payload(3, 1), "Enabled: " .. to_bool(payload(3, 1):uint(), subtree))
    subtree:add(payload(4, 2), string.format("Start: %02d:%02d", payload(4, 1):uint(), payload(5, 1):uint()))
    subtree:add(payload(6, 2), string.format("End: %02d:%02d", payload(6, 1):uint(), payload(7, 1):uint()))
end

function parse_expose_hr_apps(payload, subtree)
    assert_length(subtree, payload, 4)
    assert_zero(subtree, payload, 2)
    subtree:add(payload(3, 1), "Enabled: " .. to_bool(payload(3, 1):uint(), subtree))
end

function parse_goal_notification(payload, subtree)
    assert_length(subtree, payload, 4)
    assert_zero(subtree, payload, 2)
    subtree:add(payload(3, 1), "Enabled: " .. to_bool(payload(3, 1):uint(), subtree))
end

function parse_rotate_wrist_switch_info(payload, subtree)
    assert_length(subtree, payload, 4)
    assert_zero(subtree, payload, 2)
    subtree:add(payload(3, 1), "Enabled: " .. to_bool(payload(3, 1):uint(), subtree))
end

function parse_display_caller(payload, subtree)
    assert_length(subtree, payload, 5)
    assert_zero(subtree, payload, 2)
    assert_zero(subtree, payload, 3)
    subtree:add(payload(4, 1), "Enabled: " .. to_bool(payload(4, 1):uint(), subtree))
end

display_commands = {
    [0x01] = { "BT Connected Advertise", parse_bt_connected_advertise },
    [0x03] = { "Distance Unit", parse_distance_unit },
    [0x0a] = { "Date Display", parse_date_display },
    [0x1e] = { "Date Format", parse_date_format },
    [0x02] = { "Time Format", parse_time_format },
    [0x0c] = { "Disconnect Notification", parse_disconnect_notification },
    [0x1f] = { "Expose HR 3rd party apps", parse_expose_hr_apps },
    [0x06] = { "Goal Notification", parse_goal_notification },
    [0x0d] = { "Rotate Wrist to Switch Info", parse_rotate_wrist_switch_info },
    [0x10] = { "Display Caller", parse_display_caller },
    [0x16] = { "Screen Unlock", parse_screen_unlock },
    [0x17] = { "Language", parse_language },
    [0x20] = { "Restrict Pairing", parse_restrict_pairing },
    [0x22] = { "Activity Monitoring", parse_activity_monitoring },
    [0x1a] = { "Heart Rate Alert", parse_heart_rate_alert },
    [0x05] = { "Lift Wrist", parse_lift_wrist },
    [0x19] = { "Phone Silent Mode", parse_phone_silent_mode },
    [0x23] = { "Lift Wrist Response Speed", parse_lift_wrist_response_speed },
    [0x21] = { "Password", parse_password },
}

function parse_display(payload, pinfo, subtree)
    local payload_type_code = payload(1, 1):uint()

    if display_commands[payload_type_code] == nil then
        pinfo.cols.info = string.format("Display - Unknown (0x%x)", payload_type_code)

        on_warn(subtree, string.format("Unknown display command 0x%x", payload_type_code))
        return
    end

    local payload_type = "Unknown"
    payload_type = display_commands[payload_type_code][1]
    payload_handler = display_commands[payload_type_code][2]

    pinfo.cols.info = "Display - " .. payload_type

    if (type(payload_handler) == "table") then
        -- There's a subtype
        local payload_subtype_code = payload(1,1):uint()

        if payload_handler[payload_subtype_code] == nil then
            -- Unknown payload subtype
            pinfo.cols.info:append(string.format(", unknown subtype (0x%x)", payload_subtype_code))

            on_warn(subtree, string.format("Unknown display command subtype 0x%x", payload_subtype_code))
            return
        end

        payload_subtype = payload_handler[payload_subtype_code][1]
        payload_handler = payload_handler[payload_subtype_code][2]

        pinfo.cols.info:append(", " .. payload_subtype)
    end

    if (type(payload_handler) == "function") then
        payload_handler(payload, subtree)
    else
        on_warn(subtree, "Unhandled payload")
    end
end

function parse_handle_configuration_response(payload, pinfo, subtree)
    local configuration_endpoint = payload(1, 1):uint()
    local payload_type_code = payload(2, 1):uint()

    if configuration_endpoint == 0x06 then
        assert_length(subtree, payload, 5)
        assert_zero(subtree, payload, 3)

        payload_type = display_commands[payload_type_code][1]

        if payload_type ~= nil then
            subtree:add(payload(2, 1), "Type: " .. payload_type)
            subtree:add(payload(4, 1), "Success: " .. to_bool(payload(4, 1):uint(), subtree))
            pinfo.cols.info = 'ACK - ' .. payload_type
            if payload(4, 1):uint() ~= 1 then
                on_warn(subtree, "Not success")
            end
        else
            on_warn(subtree, "Unknown payload type for " .. payload_type_code)
        end
    elseif configuration_endpoint == 0x0a then
        assert_length(subtree, payload, 3)
        subtree:add(payload(2, 1), "Type: Display Items (old)")
        subtree:add(payload(2, 1), "Success: " .. to_bool(payload(2, 1):uint(), subtree))
        pinfo.cols.info = 'ACK - Display Items (old)'
        if payload(2, 1):uint() ~= 1 then
            on_warn(subtree, "Not success")
        end
    elseif configuration_endpoint == 0x1a then
        assert_length(subtree, payload, 3)
        subtree:add(payload(2, 1), "Type: Night Mode")
        subtree:add(payload(2, 1), "Success: " .. to_bool(payload(2, 1):uint(), subtree))
        pinfo.cols.info = 'ACK - Night Mode'
        if payload(2, 1):uint() ~= 1 then
            on_warn(subtree, "Not success")
        end
    elseif configuration_endpoint == 0xfe and payload_type_code == 0x06 then
        assert_length(subtree, payload, 5)
        subtree:add(payload(2, 1), "Type: Stress Monitoring")
        subtree:add(payload(4, 1), "Success: " .. to_bool(payload(4, 1):uint(), subtree))
        pinfo.cols.info = 'ACK - Stress Monitoring'
        if payload(4, 1):uint() ~= 1 then
            on_warn(subtree, "Not success")
        end
    elseif configuration_endpoint == 0x08 then
        assert_length(subtree, payload, 3)
        subtree:add(payload(1, 1), "Type: Inactivity Warnings")
        subtree:add(payload(2, 1), "Success: " .. to_bool(payload(2, 1):uint(), subtree))
        pinfo.cols.info = 'ACK - Inactivity Warnings'
        if payload(2, 1):uint() ~= 1 then
            on_warn(subtree, "Not success")
        end
    elseif configuration_endpoint == 0x09 then
        assert_length(subtree, payload, 3)
        subtree:add(payload(1, 1), "Type: Do Not Disturb")
        pinfo.cols.info = 'ACK - Do Not Disturb'
        if payload(2, 1):uint() == 0x05 then
            subtree:add(payload(2, 1), "Feature not Available")
        else
            subtree:add(payload(2, 1), "Success: " .. to_bool(payload(2, 1):uint(), subtree))
        end
    elseif configuration_endpoint == 0x0d then
        pinfo.cols.info = 'ACK - Alarms'
        on_warn(subtree, "TODO: Parse alarms")
    elseif configuration_endpoint == 0x0e then
        pinfo.cols.info = 'ACK - GPS Version'
        subtree:add(payload(1, 1), "Type: GPS Version")

        if payload(2, 1):uint() == 0x05 then
            subtree:add(payload(2, 1), "GPS not available")
        elseif payload(2, 1):uint() == 0x01 then
            subtree:add(payload(2, 1), "GPS: Available")
            subtree:add(payload(3, payload:len() - 3), "Version: " .. payload(3, payload:len() - 3):string())
        else
            on_warn(subtree, string.format("Unknown GPS version state 0x%x", payload(2, 1):uint()))
        end
    else
        on_warn(subtree, string.format("Unhandled configuration response 0x%x", configuration_endpoint))
    end
end

function parse_handle_configuration(payload, pinfo, subtree)
    local configuration_endpoint = payload(0, 1):uint()

    if configuration_endpoint == 0x06 then
        pinfo.cols.info = 'Display'
        parse_display(payload, pinfo, subtree)
    elseif configuration_endpoint == 0x0a then
        pinfo.cols.info = 'Display Items (old)'
        parse_display_items_old(payload, pinfo, subtree)
    elseif configuration_endpoint == 0x0d then
        pinfo.cols.info = 'Request Alarms'
        assert_length(subtree, payload, 1)
    elseif configuration_endpoint == 0x0e then
        pinfo.cols.info = 'Request GPS Version'
        assert_length(subtree, payload, 1)
    elseif configuration_endpoint == 0x1a then
        pinfo.cols.info = 'Config - Night Mode'
        parse_night_mode(payload, pinfo, subtree)
    elseif configuration_endpoint == 0x08 then
        pinfo.cols.info = 'Config - Inactivity Warnings'
        parse_inactivity_warnings(payload, pinfo, subtree)
    -- elseif configuration_endpoint == 0xff then
    -- Value: ff11000000 request workout types
    elseif configuration_endpoint == 0x09 then
        pinfo.cols.info = 'Config - Do Not Disturb'
        parse_do_not_disturb(payload, pinfo, subtree)
    elseif configuration_endpoint == 0xfe and payload(1, 1):uint() == 0x06 then
        pinfo.cols.info = 'Stress Monitoring'
        assert_length(subtree, payload, 4)
        assert_zero(subtree, payload, 2)
        subtree:add(payload(3, 1), "Enabled: " .. to_bool(payload(3, 1):uint(), subtree))
    elseif configuration_endpoint == 0x10 then
        pinfo.cols.info = 'Configuration Response'
        parse_handle_configuration_response(payload, pinfo, subtree)
    elseif configuration_endpoint == 0x80 then
        pinfo.cols.info = 'Chunked Configuration Response'
        -- This is actually a response
        on_warn(subtree, "TODO: Parse chunked configuration response")
    elseif configuration_endpoint == 0xff then
        pinfo.cols.info = 'Request Config'
        parse_request_config(payload, pinfo, subtree)
    else
        on_warn(subtree, string.format("Unhandled configuration 0x%x", configuration_endpoint))
    end
end

--
-- Chunked
--

function parse_reminder(payload, pinfo, subtree)
    position = payload(1, 1):uint()
    flags = payload(2, 1):uint()

    if payload:len() == 7 then
        pinfo.cols.info = string.format("Reminder - delete %d", position)
        assert_zero(subtree, payload, 3)
        assert_zero(subtree, payload, 4)
        assert_zero(subtree, payload, 5)
        assert_zero(subtree, payload, 6)
    else
        -- TODO parse reminder
        on_warn(subtree,"TODO: Parse reminders")
    end

end

function parse_phone_volume(payload, subtree)
    assert_length(subtree, payload, 2)

    subtree:add(payload(1, 1), "Volume: " .. payload(1, 1):uint() .. "%")
end

-- from Gadgetbridge HuamiIcon.java
huami_icons = {
    [0] = "WECHAT",
    [1] = "PENGUIN_1",
    [2] = "MI_CHAT_2",
    [3] = "FACEBOOK",
    [4] = "TWITTER",
    [5] = "MI_APP_5",
    [6] = "SNAPCHAT",
    [7] = "WHATSAPP",
    [8] = "RED_WHITE_FIRE_8",
    [9] = "CHINESE_9",
    [10] = "ALARM_CLOCK",
    [11] = "APP_11",
    [12] = "INSTAGRAM",
    [13] = "CHAT_BLUE_13",
    [14] = "COW_14",
    [15] = "CHINESE_15",
    [16] = "CHINESE_16",
    [17] = "STAR_17",
    [18] = "APP_18",
    [19] = "CHINESE_19",
    [20] = "CHINESE_20",
    [21] = "CALENDAR",
    [22] = "FACEBOOK_MESSENGER",
    [23] = "VIBER",
    [24] = "LINE",
    [25] = "TELEGRAM",
    [26] = "KAKAOTALK",
    [27] = "SKYPE",
    [28] = "VKONTAKTE",
    [29] = "POKEMONGO",
    [30] = "HANGOUTS",
    [31] = "MI_31",
    [32] = "CHINESE_32",
    [33] = "CHINESE_33",
    [34] = "EMAIL",
    [35] = "WEATHER",
    [36] = "HR_WARNING_36",
}

function parse_notification(payload, subtree)
    if payload:len() == 6 then
        if payload(1, 1):uint() == 2 then
            subtree:add(payload(1, 1), "Action: dismiss from phone?")
            -- TODO Confirm
            on_warn(subtree, 'WIP, this is not confirmed')
        else
            assert_zero(subtree, payload, 1)
        end

        assert_zero(subtree, payload, 2)
        assert_zero(subtree, payload, 3)
        assert_zero(subtree, payload, 4)
        assert_zero(subtree, payload, 5)

        return
    end

    assert_zero(subtree, payload, 1)
    assert_zero(subtree, payload, 2)
    assert_zero(subtree, payload, 3)
    assert_zero(subtree, payload, 4)

    -- TODO Confirm
    subtree:add(payload(5, 1), "Show Text?: " .. to_bool(payload(5, 1):uint(), subtree))

    if huami_icons[payload(6, 1):uint()] ~= nil then
        customIconId = huami_icons[payload(6, 1):uint()]
    else
        customIconId = string.format("Unknown (0x%x)", payload(6, 1):uint())
        on_error(subtree, string.format("Unknown icon id 0x%x", payload(6, 1):uint()))
    end

    title = get_string(payload, 7)
    message_start = 7 + title:len() + 1
    message = get_string(payload, message_start)
    app_start = message_start + message:len() + 1
    app = get_string(payload, app_start)

    subtree:add(payload(6, 1), "Icon: " .. customIconId)
    subtree:add(payload(7, title:len() + 1), "Title: " .. title)
    subtree:add(payload(message_start, message:len() + 1), "Message: " .. message)
    subtree:add(payload(app_start, app:len() + 1), "App: " .. app)
end

function parse_vibration(payload, subtree)
    notification_types = {
        [0x00] = "APP_ALERTS",
        [0x01] = "INCOMING_CALL",
        [0x02] = "INCOMING_SMS",
        [0x04] = "GOAL_NOTIFICATION",
        [0x05] = "ALARM",
        [0x06] = "IDLE_ALERTS",
        [0x08] = "EVENT_REMINDER",
        [0x09] = "FIND_BAND";
    }

    subtree:add(payload(1, 1), "Notification Type: " .. notification_types[payload(1, 1):uint()])

    if bit32.band(payload(2, 1):uint(), 0x80) > 0 then
        subtree:add(payload(2, 1), "Test: true")
    else
        subtree:add(payload(2, 1), "Test: false")
    end

    numOnOff = bit32.band(payload(2, 1):uint(), bit32.bnot(0x80), bit32.bnot(0x40))
    subtree:add(payload(2, 1), "Num On Off: " .. numOnOff)

    assert_length(subtree, payload, numOnOff * 4 + 3)

    local subtree_pattern = subtree:add(mi_proto, buffer, "Vibration Pattern")

    local i = 3
    while i < payload:len() - 1 do
        subtree_pattern:add(payload(i, 2), "On: " .. payload(i, 2):le_uint() .. "ms")
        subtree_pattern:add(payload(i + 2, 2), "Off: " .. payload(i + 2, 2):le_uint() .. "ms")

        i = i + 4
    end
end

-- from Gadgetbridge HuamiMenuType.java
display_items = {
    [0x01] = "status",
    [0x02] = "hr",
    [0x03] = "workout",
    [0x04] = "weather",
    [0x06] = "notifications",
    [0x07] = "more",
    [0x08] = "dnd",
    [0x09] = "alarm",
    [0x0a] = "takephoto",
    [0x0b] = "music",
    [0x0c] = "stopwatch",
    [0x0d] = "timer",
    [0x0e] = "findphone",
    [0x0f] = "mutephone",
    [0x10] = "nfc",
    [0x11] = "alipay",
    [0x12] = "watchface",
    [0x13] = "settings",
    [0x14] = "activity",
    [0x15] = "eventreminder",
    [0x16] = "compass",
    [0x19] = "pai",
    [0x1a] = "worldclock",
    [0x1b] = "timer_stopwatch",
    [0x1c] = "stress",
    [0x1d] = "period",
    [0x21] = "goal",
    [0x23] = "sleep",
    [0x24] = "spo2",
    [0x26] = "events",
    [0x28] = "widgets",
    [0x33] = "breathing",
    [0x34] = "steps",
    [0x35] = "distance",
    [0x36] = "calories",
    [0x38] = "pomodoro",
    [0x39] = "alexa",
    [0x3a] = "battery",
    [0x40] = "temperature",
    [0x41] = "barometer",
    [0x43] = "flashlight",
}

function parse_display_items(payload, subtree)
    menu_types = {
        [0xfd] = "Shortcut",
        [0xff] = "MenuItem",
    }

    local i = 1
    while i < payload:len() - 1 do
        if payload(i + 1, 1):uint() == 0 then
            state_str = " (enabled)"
        elseif payload(i + 1, 1):uint() == 1 then
            state_str = " (disabled)"
        else
            on_error(subtree, "Unknown screen state " .. payload(i + 1, 1):uint())
        end

        local idx = payload(i, 1):uint()
        local menu_type = menu_types[payload(i + 2, 1):uint()]
        local screen_id = payload(i + 3, 1):uint()

        subtree:add(payload(i, 4), menu_type .. "[" .. idx .. "] = " .. display_items[screen_id] .. state_str)

        i = i + 4
    end
end

workout_screens = {
    [0x01] = "Outdoor running",
    [0x06] = "Walking",
    [0x08] = "Treadmill",
    [0x09] = "Outdoor cycling",
    [0x0a] = "Indoor cycling",
    [0x0c] = "Elliptical",
    [0x0e] = "Pool swimming",
    [0x10] = "Freestyle",
    [0x15] = "Jump rope",
    [0x17] = "Rowing machine",
    [0x3c] = "Yoga",
}

function parse_workout_screens(payload, subtree)
    assert_zero(subtree, payload, 1)

    local i = 2
    while i < payload:len() - 1 do
        assert_zero(subtree, payload, i + 1)

        local state_str = " (unknown)"
        if payload(i + 2, 1):uint() == 0 then
            state_str = " (disabled)"
        elseif payload(i + 2, 1):uint() == 1 then
            state_str = " (enabled)"
        else
            on_error(subtree, "Unknown workout type screen state " .. payload(i + 2, 1):uint())
        end

        subtree:add(payload(i, 3), "Workout type: " .. workout_screens[payload(i, 1):uint()] .. state_str)

        i = i + 3
    end
end

function parse_gps_location(payload, subtree)
    local flags = payload(0, 4):le_int()

    local flags_txt = ""

    for i = 0, 32 do
        if bit32.band(flags, 2^i) > 0 then
            flags_txt = flags_txt .. " " .. string.format("0x%x", 2^i)

            if i ~= 0 and i ~= 18 then
                -- TODO: We currently only handle 0x01 and 0x40000, but there are more (eg. if we start the workout from the phone instead of the band, the payloads contain much more data)
                on_warn(subtree, "Unknown gps location flag: " .. string.format("0x%x", 2^i))
            end
        end
    end

    subtree:add(payload(0, 4), "Flags:" .. flags_txt)

    local i = 4

    if bit32.band(flags, 0x01) > 0 then
        local gps_state = payload(i, 1):le_int()

        if gps_state == 0x02 then
            subtree:add(payload(i, 1), "GPS State: searching")
        elseif gps_state == 0x01 then
            subtree:add(payload(i, 1), "GPS State: acquired")
        elseif gps_state == 0x04 then
            subtree:add(payload(i, 1), "GPS State: disabled")
        else
            subtree:add(payload(i, 1), "GPS State: unknown")
            on_error(subtree, "Unknown gps state " .. gps_state)
        end

        i = i + 1
    end

    if bit32.band(flags, 0x40000) > 0 then
        local lon = payload(i, 4):le_int() / 3000000.0
        local lat = payload(i + 4, 4):le_int() / 3000000.0
        -- For the speed
        -- int | band
        -- 15  | 5.58 km/h
        -- 20  | 7.38 km/h
        -- 35  | 12.78 km/h
        -- 52  | 18.90 km/h
        local speed = (payload(i + 8, 4):le_int() / 10.0) * 3.6
        local altitude = payload(i + 12, 4):le_int() / 100.0
        local timestamp = payload(i + 16, 8):le_int64()

        -- TODO: ff ff ff ff always after timestamp?
        assert_value(subtree, payload, i + 24, 0xff)
        assert_value(subtree, payload, i + 25, 0xff)
        assert_value(subtree, payload, i + 26, 0xff)
        assert_value(subtree, payload, i + 27, 0xff)

        local bearing = payload(i + 28, 2):le_int() -- TODO Not sure?

        --  TODO zero at the end?
        assert_zero(subtree, payload, i + 30)

        subtree:add(payload(i, 4), "Lon: " .. lon)
        subtree:add(payload(i + 4, 4), "Lat: " .. lat)
        subtree:add(payload(i + 8, 4), "Speed: " .. speed .. " km/h")
        subtree:add(payload(i + 12, 4), "Altitude: " .. altitude)
        subtree:add(payload(i + 16, 8), "Timestamp: " .. os.date("%Y/%m/%d %H:%M:%S", (timestamp / 1000):tonumber()))
        subtree:add(payload(i + 28, 2), "Bearing?: " .. bearing) -- TODO Not sure

        i = i + 31
    end

    if i ~= payload:len() then
        on_error(subtree, "Unexpected payload length " .. payload:len())
    end
end

-- Store all chunked frames so we can handle chunks
local all_chunk_frames = {}

function parse_handle_chunked(payload, pinfo, subtree)
    if all_chunk_frames[frame_number().value] == nil then
        all_chunk_frames[frame_number().value] = payload:bytes()
    end

    if payload(0, 1):uint() == 0x00 then
        local chunk_flags = payload(1, 1):uint()
        local chunk_type = bit32.band(chunk_flags, 0x3f)
        local chunk_num = payload(2, 1):uint()

        pinfo.cols.info = string.format("Chunk part %d", chunk_num)
        subtree:add(payload(1, 1), string.format("Flags: 0x%x", chunk_flags))
        subtree:add(payload(2, 1), string.format("Num: %d", chunk_num))
        subtree:add(payload(1, 1), string.format("Type: 0x%x", chunk_type))

        if bit32.band(chunk_flags, 0x80) == 0 then
            -- Split chunk / not last chunk
            return
        end

        local buffer = ByteArray.new()
        local expected_chunk = chunk_num

        for n = frame_number().value, 0, -1 do
            local chunk = all_chunk_frames[n]

            if chunk ~= nil then
                local tvb = ByteArray.tvb(chunk, string.format("tvb_%d", n))
                buffer:prepend(chunk(3, tvb:len() - 3))

                if tvb(2, 1):uint() ~= expected_chunk then
                    on_error(subtree, "Found unexpected chunk " .. tvb(2, 1):uint())
                    return
                end

                expected_chunk = expected_chunk - 1

                if tvb(2, 1):uint() == 0 then
                    break
                end
            end
        end

        reassembled = ByteArray.tvb(buffer, "tvb")

        subtree:add(reassembled(0, reassembled:len()), string.format("Payload: %s", buffer:tohex(true, ":")))

        local chunk_command = reassembled(0, 1):uint()

        if chunk_command == 0x1e then
            pinfo.cols.info = 'Display Items'
            parse_display_items(reassembled, subtree)
        elseif chunk_type == 0x09 and chunk_command == 0x0b then
            pinfo.cols.info = 'Workout Screens'
            parse_workout_screens(reassembled, subtree)
        elseif chunk_type == 0x02 and chunk_command == 0x0b then
            pinfo.cols.info = 'Reminder'
            parse_reminder(reassembled, pinfo, subtree)
        elseif chunk_command == 0x40 then
            pinfo.cols.info = 'Phone Volume'
            parse_phone_volume(reassembled, subtree)
        elseif chunk_command == 0x20 then
            pinfo.cols.info = 'Vibration Pattern'
            parse_vibration(reassembled, subtree)
        elseif chunk_command == 0xfa then
            pinfo.cols.info = 'Notification'
            parse_notification(reassembled, subtree)
        elseif chunk_command == 0x06 then
            pinfo.cols.info = 'GPS Location'
            parse_gps_location(reassembled(1, reassembled:len() - 1), subtree)
        elseif chunk_type == 0x01  and chunk_command == 0x02 then
            pinfo.cols.info = 'Weather'
            -- TODO
        elseif workout_status_types[chunk_command] ~= nil then
            pinfo.cols.info = 'WORKOUT_COMMAND_FROM_PHONE'
            status_type = workout_status_types[chunk_command]

            subtree:add(payload(1, 1), "Workout Status: " .. status_type)
            on_warn(subtree, "WIP experiental")
        else
            on_warn(subtree, string.format("Unhandled chunk type 0x%x, cmd 0x%x", chunk_type, chunk_command))
        end
    elseif payload(0, 1):uint() == 0x10 then
        pinfo.cols.info = 'Chunked Response'

        assert_length(subtree, payload, 7)
        assert_value(subtree, payload, 1, 0)
        assert_value(subtree, payload, 3, 1)
        assert_value(subtree, payload, 4, 1)
        assert_value(subtree, payload, 5, 0)
        assert_value(subtree, payload, 6, 0)

        local chunk_flags = payload(2, 1):uint()
        local chunk_type = bit32.band(chunk_flags, 0x3f)

        subtree:add(payload(2, 1), string.format("Flags: 0x%x", chunk_flags))
        subtree:add(payload(2, 1), string.format("Type: 0x%x", chunk_type))
    end
end

-- Store all chunked frames so we can handle chunks
local all_chunk_2021_frames = {}

function parse_handle_chunked_2021(payload, pinfo, subtree)
    extended_flags = false  -- TODO: determine this from device

    pinfo.cols.info = 'CHUNKED_2021'

    if all_chunk_2021_frames[frame_number().value] == nil then
        all_chunk_2021_frames[frame_number().value] = payload:bytes()
    end

    if payload(0, 1):uint() == 0x03 then
        local chunk_flags = payload(1, 1):uint()

        if bit32.band(chunk_flags, 0x08) > 0 then
            on_error(subtree, "Encrypted 2021 chunks are not supported yet")
            return
        end

        local chunk_handle = payload(2, 1):uint()
        local chunk_num = payload(3, 1):uint()

        if extended_flags then
            chunk_handle = payload(3, 1):uint()
            chunk_num = payload(4, 1):uint()
        end

        pinfo.cols.info = string.format("Chunk 2021 part %d", chunk_num)
        subtree:add(payload(1, 1), string.format("Flags: 0x%x", chunk_flags))
        subtree:add(payload(2, 1), string.format("Num: %d", chunk_num))

        if bit32.band(chunk_flags, 0x02) == 0 then
            -- Split chunk / not last chunk
            return
        end

        local buffer = ByteArray.new()
        local expected_chunk = chunk_num
        local full_lenth = nil
        local chunk_type = nil

        for n = frame_number().value, 0, -1 do
            local chunk = all_chunk_2021_frames[n]

            if chunk ~= nil then
                local tvb = ByteArray.tvb(chunk, string.format("tvb_%d", n))

                if tvb(0, 1):uint() ~= 3 then
                    on_error(subtree, string.format("Found unexpected first byte on chunk frame number %d", n))
                    return
                end

                local i = 4
                if extended_flags then
                    i = i + 1
                end

                if tvb(3, 1):uint() == 0 then
                    full_lenth = tvb(i, 4):le_int()
                    chunk_type = tvb(i + 4, 2):le_int()

                    i = i + 6
                end

                buffer:prepend(chunk(i, chunk:len() - i))

                if tvb(3, 1):uint() ~= expected_chunk then
                    on_error(subtree, string.format("Found unexpected chunk %d, frame number %d", tvb(3, 1):uint(), n))
                    return
                end

                expected_chunk = expected_chunk - 1

                if tvb(3, 1):uint() == 0 then
                    break
                end
            end
        end

        reassembled = ByteArray.tvb(buffer, "tvb")

        if full_lenth ~= reassembled:len() then
            on_error(subtree, string.format("Reassembled length %d, expected %d", reassembled:len(), full_lenth))
            return
        end

        subtree:add(reassembled(0, reassembled:len()), string.format("Payload: %s", buffer:tohex(true, ":")))

        if chunk_type == 0x0008 and reassembled(0, 1):uint() == 0x03 then
            pinfo.cols.info = 'World Clocks'
            -- TODO: Parse
        else
            on_warn(subtree, "Unhandled reassembled chunked 2021 frame")
        end
    else
        on_warn(subtree, "Unhandled chunk 2021 frame")
    end
end

--
-- Heart Rate
--

function parse_handle_hr_control_point(payload, pinfo, subtree)
    local point = payload(0, 1):uint()
    if point == 0x14 then
        pinfo.cols.info = "SET_PERIODIC_HR_MEASUREMENT_INTERVAL"

        subtree:add(payload(0, 1), "HR Control Point: COMMAND_SET_PERIODIC_HR_MEASUREMENT_INTERVAL")
        subtree:add(payload(1, 1), "Interval: " .. payload(1, 1):uint() .. " minutes")

        assert_length(subtree, payload, 2)
    elseif point == 0x15 then
        pinfo.cols.info = "HR_SLEEP_MEASUREMENT"

        subtree:add(payload(0, 1), "HR Control Point: HR_SLEEP_MEASUREMENT")
        subtree:add(payload(2, 1), "Enabled: " .. to_bool(payload(2, 1):uint(), subtree))

        assert_zero(subtree, payload, 1)
        assert_length(subtree, payload, 3)
    else
        on_error(subtree, "Unknown hr control point " .. point)
    end
end

--
-- Auth
--

function parse_handle_auth(payload, pinfo, subtree)
    cmd = payload(0, 1):uint()
    if cmd == 0x82 then
        pinfo.cols.info = "Auth - Request Random Number"
        assert_length(subtree, payload, 5)
        assert_value(subtree, payload, 2, 2)
        assert_value(subtree, payload, 3, 1)
        assert_value(subtree, payload, 4, 0)
        authFlags = payload(1, 1):uint()

        subtree:add(payload(1, 1), string.format("Auth Flags: 0x%x", authFlags))
    elseif cmd == 0x83 then
        pinfo.cols.info = "Auth - Encrypted Random Number"
        authFlags = payload(1, 1):uint()
        subtree:add(payload(1, 1), string.format("Auth Flags: 0x%x", authFlags))
        subtree:add(payload(2, payload:len() - 2), "Encrypted Random Number: " .. payload(2, payload:len() - 2):bytes():tohex(false, ':'))
        -- TODO handle?
    elseif cmd == 0x02 then
        pinfo.cols.info = "Auth - Request Random Number (encrypted)"
        assert_length(subtree, payload, 2)
        authFlags = payload(1, 1):uint()
        subtree:add(payload(1, 1), string.format("Auth Flags: 0x%x", authFlags))
    elseif cmd == 0x10 then
        pinfo.cols.info = "Auth - Response"

        response_type = bit32.band(payload(1, 1):uint(), 0xf)

        if response_type == 0x01 then
            pinfo.cols.info = "Auth - AUTH_SEND_KEY"
            -- TODO handle?
        elseif response_type == 0x02 then
            pinfo.cols.info = "Auth - Random Number"
            subtree:add(payload(2, 1), "Success: " .. to_bool(payload(2, 1):uint(), subtree))
            subtree:add(payload(3, payload:len() - 3), "Random Number: " .. payload(3, payload:len() - 3):bytes():tohex(false, ':'))
        elseif response_type == 0x03 then
            pinfo.cols.info = "Auth - ACK"
            assert_length(subtree, payload, 3)
            subtree:add(payload(2, 1), "Success: " .. to_bool(payload(2, 1):uint(), subtree))
        else
            on_error(subtree, string.format("Unknown auth response 0x%x", response_type))
        end
    else
        on_error(subtree, string.format("Unknown auth cmd 0x%x", cmd))
    end
end

--
-- User Settings
--

function parse_user_settings(payload, pinfo, subtree)
    local setting_code = payload(0, 1):uint()
    pinfo.cols.info = "User Setttings"

    if setting_code == 0x20 then
        pinfo.cols.info:append(" - Band Wrist")

        assert_length(subtree, payload, 4)
        assert_zero(subtree, payload, 1)
        assert_zero(subtree, payload, 2)

        if payload(3, 1):uint() == 0x02 then
            subtree:add(payload(3, 1), "Band Wrist: left")
        elseif payload(3, 1):uint() == 0x82 then
            subtree:add(payload(3, 1), "Band Wrist: right")
        else
            on_error(subtree, "Unexpected wrist " .. payload(3, 1):uint())
        end
    elseif setting_code == 0x10 then
        pinfo.cols.info:append(" - Fitness Goal")
        assert_length(subtree, payload, 7)
        assert_zero(subtree, payload, 1)
        assert_zero(subtree, payload, 2)
        assert_zero(subtree, payload, 5)
        assert_zero(subtree, payload, 6)
        subtree:add(payload(3, 2), "Steps goal: " .. payload(3, 2):le_int())
    else
        on_error(subtree, string.format("Unknown user setting 0x%x", setting_code))
    end
end

--
-- Device Events
--

workout_types = {
    [0x01] = "Outdoor running",
    [0x04] = "Walking",
    [0x02] = "Treadmill",
    [0x03] = "Outdoor cycling",
    [0x09] = "Indoor cycling",
    [0x05] = "Pool swimming",
    [0x0b] = "Freestyle",
    [0x06] = "Elliptical",
    [0x08] = "Jump rope",
    [0x07] = "Rowing machine",
    [0x0a] = "Yoga",
}

function parse_device_event(payload, pinfo, subtree)
    local event_code = payload(0, 1):uint()

    if event_code == 0x14 then
        pinfo.cols.info = "Workout - Open"

        assert_length(subtree, payload, 4)
        assert_zero(subtree, payload, 1)

        workout_type = workout_types[payload(3, 1):uint()]

        subtree:add(payload(3, 1), "Workout Type: " .. workout_type)
        subtree:add(payload(2, 1), "GPS: " .. to_bool(payload(2, 1):uint(), subtree))
    elseif event_code == 0x0e then
        pinfo.cols.info = "Tick 30 min"
        assert_length(subtree, payload, 1)
    elseif event_code == 0x16 then
        pinfo.cols.info = "MTU Request"

        assert_length(subtree, payload, 3)
        assert_zero(subtree, payload, 2)

        subtree:add(payload(1, 1), "MTU: " .. payload(1, 1):uint(), subtree)
    elseif event_code == 0x10 then
        pinfo.cols.info = "Silent Mode - Set from band"
        assert_length(subtree, payload, 2)
        subtree:add(payload(1, 1), "Silent Mode: " .. to_bool(payload(1, 1):uint(), subtree))
    else
        pinfo.cols.info = string.format("Device Event - Unknown (0x%x)", event_code)
        on_warn(subtree, string.format("Unknown device event 0x%x", event_code))
    end
end

function parse_realtime_steps(payload, pinfo, subtree)
    pinfo.cols.info = "Realtime Steps"
    assert_length(subtree, payload, 13)

    -- TODO: what are the rest of the bytes? mostly zeroes

    subtree:add(payload(1, 2), "Number of steps: " .. payload(1, 2):le_uint())
end

workout_status_types = {
    [0x02] = "Start",
    [0x03] = "Pause",
    [0x04] = "Resume",
    [0x05] = "End",
}

function parse_workout_state(payload, pinfo, subtree)
    if payload(0, 1):uint() == 0x01 then
        pinfo.cols.info = "Workout - Start from Phone"
        assert_length(subtree, payload, 3)
        workout_type = workout_types[payload(2, 1):uint()]
        subtree:add(payload(1, 1), "Workout Type: " .. workout_type)

        -- TODO payload(1, 0)
    elseif payload(0, 1):uint() == 0x11 then
        pinfo.cols.info = "Workout Status"

        assert_length(subtree, payload, 2)

        status_type = workout_status_types[payload(1, 1):uint()]

        subtree:add(payload(1, 1), "Workout Status: " .. status_type)
    elseif payload(0, 1):uint() == 0x10 then
        pinfo.cols.info = "Workout Status - ACK"

        assert_length(subtree, payload, 3)

        status_type = workout_status_types[payload(1, 1):uint()]

        subtree:add(payload(1, 1), "Workout Status: " .. status_type)
        subtree:add(payload(2, 1), "Success: " .. to_bool(payload(2, 1):uint(), subtree))
    else
        on_error(subtree, "Unknown Workout State " .. payload(0, 1):uint())
    end
end

battery_states = {
    [0x00] = "Normal",
    [0x01] = "Charging",
}

function parse_handle_battery(payload, pinfo, subtree)
    pinfo.cols.info = "Battery Info"
    subtree:add(payload(1, 1), "Battery: " .. payload(1, 1):uint() .. "%")
    subtree:add(payload(2, 1), "State: " .. battery_states[payload(2, 1):uint()])

    if payload:len() > 3 then
        assert_length(subtree, payload, 20)
        -- TODO Rest of bytes
        -- TODO GB reads from offset 10?
        subtree:add(payload(11, 8), "Last Charge: " .. parse_raw_bytes_timestamp(payload, subtree, 11))
        subtree:add(payload(19, 1), "Last Charge: " .. payload(19, 1):uint() .. "%")
    end
end

--
-- Handles
--

HANDLES = {
    -- [UUID: Heart Rate Control Point (0x2a39)]
    [0x002c] = { "HANDLE_HR_CONTROL_POINT", parse_handle_hr_control_point },

    -- [UUID: Heart Rate Measurement (0x2a37)]
    [0x0029] = { "HANDLE_HR_MEASUREMENT", parse_handle_hr_measurement },

    -- [UUID: 000000200000351221180009af100700]
    [0x0050] = { "UUID_CHARACTERISTIC_CHUNKEDTRANSFER", parse_handle_chunked },

    -- [UUID: 000000030000351221180009af100700]
    [0x0038] = { "UUID_CHARACTERISTIC_3_CONFIGURATION", parse_handle_configuration },

    -- [UUID: 000000160000351221180009af100700]
    [0x0062] = { "UUID_CHARACTERISTIC_CHUNKEDTRANSFER_2021_WRITE", parse_handle_chunked_2021 },

    -- [UUID: 000000010000351221180009af100700]
    [0x0032] = { "UUID_UNKNOWN_RAW_SENSOR_CONTROL", parse_handle_noop },

    -- [UUID: 000000020000351221180009af100700]
    [0x0035] = { "UUID_UNKNOWN_RAW_SENSOR_DATA", parse_handle_noop },

    -- [UUID: 000000040000351221180009af100700]
    [0x003e] = { "UUID_UNKNOWN_CHARACTERISTIC4", parse_fetch },

    -- [UUID: 000000140000351221180009af100700]
    [0x0015] = { "UUID_UNKNOWN_CHARACTERISTIC5", parse_handle_noop },

    -- [UUID: 000000090000351221180009af100700]
    [0x0069] = { "UUID_CHARACTERISTIC_AUTH", parse_handle_auth },

    -- [UUID: 000000080000351221180009af100700]
    [0x004a] = { "UUID_CHARACTERISTIC_8_USER_SETTINGS", parse_user_settings },

    -- [UUID: 000000100000351221180009af100700]
    [0x004d] = { "UUID_CHARACTERISTIC_DEVICEEVENT", parse_device_event },

    -- [UUID: 000000100000351221180009af100700]
    [0x0047] = { "UUID_CHARACTERISTIC_7_REALTIME_STEPS", parse_realtime_steps },

    -- [UUID: 000000060000351221180009af100700]
    [0x0044] = { "UUID_CHARACTERISTIC_6_BATTERY_INFO", parse_handle_battery },

    -- [UUID: 000000050000351221180009af100700]
    -- [Characteristic UUID: 000000050000351221180009af100700]
    [0x0041] = { "UUID_CHARACTERISTIC_5_ACTIVITY_DATA", parse_activity },
    [0x0042] = { "UUID_CHARACTERISTIC_5_ACTIVITY_DATA", parse_activity },

    -- [UUID: 000000170000351221180009af100700]
    [0x0065] = { "UUID_CHARACTERISTIC_CHUNKEDTRANSFER_2021_READ", parse_handle_chunked_2021 },

    -- [UUID: 0000000f0000351221180009af100700]
    [0x0056] = { "UUID_CHARACTERISTIC_WORKOUTSTATE", parse_workout_state },
}

--
-- Dissector base
--

opcodes = {
    -- TODO: Handle [0x12] = "Write Request",
    [0x1B] = "Handle Value Notification",
    [0x52] = "Write Command",
    -- TODO: Handle [0x0a] = "Read Request",
    [0x0b] = "Read Response",
    [0x13] = "Write Response",
}

function mi_proto.dissector(buffer, pinfo, tree)
    if opcodes[btatt_opcode().value] == nil then
        return
    end

    pinfo.cols.protocol = mi_proto.name

    local subtree_data = tree:add(mi_proto, buffer, "Huami Data")

    pinfo.cols.info = HANDLES[btatt_handle().value][1]
    HANDLES[btatt_handle().value][2](buffer, pinfo, subtree_data)
end

local btatt_handle_table = DissectorTable.get("btatt.handle")
for handle, _ in pairs(HANDLES) do
    btatt_handle_table:add(handle, mi_proto)
end
