<?php
/*  Copyright (C) 2021 Andreas Shimokawa

    This file is part of Gadgetbridge-tools.

    Gadgetbridge is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Gadgetbridge is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. */

require("typemap.php");

if ($argc < 2) {
    echo "USAGE: " . $argv[0] . " <filename>\n";
    exit;
}
$inputfile = $argv[1];
$outputdir = $inputfile . "_unpacked";
@mkdir($outputdir);

$content = file_get_contents($inputfile);

// overall header is 32 bytes
$header = substr($content, 0, 32);
$uihh = substr($header, 0, 4);

echo "[I] Check UIHH header: ";
if ($uihh == "UIHH")
    echo "OK\n";
else {
    echo "Failed\n";
    exit;
}

$header_crc32 = parse_uint32($header, 12);
$header_size = parse_uint32($header, 22);

$content = substr($content, 32);
$content_crc32 = crc32($content);

echo "[I] Check overall CRC32: ";
if ($header_crc32 == $content_crc32)
    echo "OK\n";
else {
    echo "Failed\n";
    exit;
}

echo "[I] Check overall size: ";
if ($header_size == strlen($content))
    echo "OK\n";
else {
    echo "Failed\n";
    exit;
}

while (true) {
    // file headers are 10 bytes
    $header = substr($content, 0, 10);
    $content = substr($content, 10);

    $type = ord($header[1]);
    echo "Found type $type payload\n";
    $header_size = parse_uint32($header, 2);
    $header_crc32 = parse_uint32($header, 6);
    $filecontent = substr($content, 0, $header_size);
    $filecontent_crc32 = crc32($filecontent);

    echo "[I] Check file CRC32: ";
    if ($header_crc32 == $filecontent_crc32)
        echo "OK\n";
    else {
        echo "Failed\n";
        exit;
    }

    echo "Check file size: ";
    if ($header_size == strlen($filecontent))
        echo "OK\n";
    else {
        echo "Failed\n";
        exit;
    }

    $filename = @$typemap[$type];
    if (!$filename) {
        $filename = "unknown_$type";
    }

    file_put_contents("$outputdir/$filename", $filecontent);
    if (strlen($filecontent) == strlen($content)) {
        echo "[I] All Done.\n";
        break;
    }
    $content = substr($content, strlen($filecontent));
}

function parse_uint32($header, $offset)
{
    return ord($header[$offset]) | ord($header[$offset + 1]) << 8 | ord($header[$offset + 2]) << 16 | ord($header[$offset + 3]) << 24;
}
