# Wireshark dissector for SuperCars - Shell Motorsport Racing protocol

Complete dissector for the bluetooth module.

Place it into `~/.local/lib/wireshark/plugins` . You can filter by typing
`supercars` into the filter field of Wireshark. Possible filters:

- `supercars.payload` - all supercars packets
- `supercars.control` - just the control data sent to the car
- `supercars.battery` - battery values received from the car
- `supercarsdescriptor` - UUID descriptor to get info from the BLE module, maybe firmware version?

# Requirements

Needs luagcrypt from https://github.com/Lekensteyn/luagcrypt . Make sure to
compile for your version of lua and place it into correct lib directory
(`/usr/lib/x86_64-linux-gnu/lua/5.2` on Debian).
