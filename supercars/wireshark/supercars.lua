print("hello SuperCars protocol")
supercars_proto = Proto("SuperCars",  "SuperCars Protocol")
supercars_proto_descriptor = Proto("SuperCarsDescriptor",  "SuperCars Protocol, descriptor section")

local gcrypt = require("luagcrypt")
print("Lua version: " .. _VERSION)


supercars_proto.fields.message_payload = ProtoField.bytes("supercars.payload", "Payload", base.COLON)
supercars_proto.fields.message_values = ProtoField.bytes("supercars.control", "Control", base.NONE)
supercars_proto.fields.message_battery = ProtoField.bytes("supercars.battery", "Battery", base.NONE)
supercars_proto.fields = {message_payload, message_values, message_battery}

supercars_proto_descriptor.fields.message_payload = ProtoField.bytes("supercarsdescriptor.descriptor", "Descriptors", base.NONE)
supercars_proto_descriptor.fields = {message_payload_descriptor}

function fromhex(hex)
   if string.match(hex, "[^0-9a-fA-F]") then
      error("Invalid chars in hex")
   end
   if string.len(hex) % 2 == 1 then
      error("Hex string must be a multiple of two")
   end
   local s = string.gsub(hex, "..", function(v)
   return string.char(tonumber(v, 16))
   end)
   return s
end

cipher = gcrypt.Cipher(gcrypt.CIPHER_AES256, gcrypt.CIPHER_MODE_ECB)
cipher:setkey(fromhex("34522a5b7a6e492c08090a9d8d2a23f8"))

function get_message_value(buffer)
   length = buffer:len()
   -- print(fromhex("34522a5b7a6e492c08090a9d8d2a23f8"))
   print(tostring(buffer(0, length)))
   -- local plaintext = "fsdafas"
   local data = cipher:decrypt(fromhex(tostring(buffer(0, length))))
   print("Decrypted: " .. data)
   return data
end

function hexdecode(hex)
   return (hex:gsub("%x%x", function(digits) return string.char(tonumber(digits, 16)) end))
end

function supercars_proto_descriptor.dissector(buffer, pinfo, tree)
   length = buffer:len()
   pinfo.cols.protocol = supercars_proto.name
   if length == 0 then return end
   print("descriptor buffer: " .. buffer(0, length))
   local subtree = tree:add(supercars_proto_descriptor, buffer(), "SuperCars Protocol Descriptors")
   subtree:add(supercars_proto.fields.message_payload, buffer()):append_text(", Data: ".. hexdecode(tostring(buffer(0, length))..""))
end

function supercars_proto.dissector(buffer, pinfo, tree)
   length = buffer:len()
   if length == 0 then return end
   print("buffer: " .. buffer(0, length))


   pinfo.cols.protocol = supercars_proto.name

   local subtree = tree:add(supercars_proto, buffer(), "SuperCars Protocol")

   get_message_value(buffer(0, length))

   local bytes = get_message_value(buffer(0, length))
   local dec_data = ByteArray.new(bytes, true):tvb("Decrypted data")

   subtree:add(supercars_proto.fields.message_payload, dec_data())

   print(dec_data(1,3))

   if tostring(dec_data(1,3))=="43544c" then
      subtree:add(supercars_proto.fields.message_values, dec_data()):append_text(
        " Start: " .. dec_data(0, 1) .. 
        ", MSG: "  .. hexdecode(tostring(dec_data(1, 3))) .. 
        ", FWD: "  .. dec_data(4, 1) .. 
        ", BWD: "  .. dec_data(5,1) .. 
        ", L: "    .. dec_data(6,1) .. 
        ", R: "    .. dec_data(7,1) .. 
        ", LGHT: " .. dec_data(8,1) .. 
        ", Mode: " .. dec_data(9,1) .. 
        ", Next: " .. dec_data(10, 6))

   elseif tostring(dec_data(1,3))=="564254" then
      subtree:add(supercars_proto.fields.message_battery, dec_data()):append_text(
        " Start: "    .. dec_data(0, 1) .. 
        ", MSG: "     .. hexdecode(tostring(dec_data(1, 3))).. 
        ", Battery: " .. tonumber(tostring(dec_data(4, 1)),16) .. 
        ", Rest: "    .. dec_data(5, 11))

   end

end


btatt_handle = DissectorTable.get("btatt.handle")
btatt_handle:add(0x0012,  supercars_proto)
btatt_handle:add(0x01b,  supercars_proto)
btatt_handle:add(0x016,  supercars_proto_descriptor)
btatt_handle:add(0x013,  supercars_proto_descriptor)



