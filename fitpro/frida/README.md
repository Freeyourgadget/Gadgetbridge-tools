# Frida proof of concept examples for the FitPro app

Basic usage:

```
adb root
adb push frida-server-15.0.16-android-arm /data/local/tmp/
adb shell "chmod 755 /data/local/tmp/frida-server"
adb shell "/data/local/tmp/frida-server &"
```

Then run:

```
python run_frida_script.py list_classes.js > fitpro_classes.txt
```

or

```
python run_frida_script.py hook_sports.js
```

For more info, search for Frida documentation and articles.
