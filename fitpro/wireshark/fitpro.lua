print("hello FitPro protocol")

fitpro_protocol = Proto("FitPro",  "FitPro Protocol")

message_header = ProtoField.bytes("fitpro.message_header", "Header", base.NONE)
message_header2 = ProtoField.int32("fitpro.message_header2", "Header2", base.STRING)
message_payload_size = ProtoField.int32("fitpro.message_payload_size", "PayloadSize", base.DEC)
message_payload_data = ProtoField.bytes("fitpro.message_payload_data", "PayloadData", base.COLON)
message_command = ProtoField.bytes("fitpro.message_command", "Command", base.COLON)
message_length = ProtoField.int32("fitpro.message_length", "messageLength", base.DEC)

-- Payload fields
display_ol_enabled= ProtoField.bytes("fitpro.dol.enabled" , "Display on lift" , base.NONE)
display_ol_from= ProtoField.bytes("fitpro.dol.from" , "From" , base.NONE)
display_ol_to= ProtoField.bytes("fitpro.dol.to" , "To" , base.NONE)

long_sit_enabled= ProtoField.bytes("fitpro.ls.enabled" , "Long sit" , base.NONE)
long_sit_from= ProtoField.bytes("fitpro.ls.from" , "From" , base.NONE)
long_sit_to= ProtoField.bytes("fitpro.ls.to" , "To" , base.NONE)
long_sit_code= ProtoField.bytes("fitpro.ls.code" , "Code" , base.NONE)

sports_data = ProtoField.bytes("fitpro.data.sports" , "Sports data" , base.COLON)
user_data = ProtoField.bytes("fitpro.data.user" , "User data" , base.COLON)
step_goal = ProtoField.bytes("fitpro.data.step" , "Step goal" , base.COLON)
hardware_data = ProtoField.none("fitpro.data.step" , "Hardware data: " , base.NONE)
heartrate_data = ProtoField.none("fitpro.data.heartrate" , "HeartRate data: " , base.NONE)

data = ProtoField.none("fitpro.data.data" , "Data: " , base.NONE)


fitpro_protocol.fields = {message_header,message_header2, message_length, message_command, message_payload_size, message_payload_data, display_ol_enabled, display_ol_from,display_ol_to, long_sit_code, long_sit_to,long_sit_from,long_sit_enabled, sports_data, user_data, step_goal, hardware_data, heartrate_data, data}


function get_command_name(command)
    local command_name = "Unknown"
    print(command)

    if command == "120101" then command_name = "CMD_SET_DATE_TIME"
    elseif command == "120102" then command_name = "CMD_SET_ALARM"
    elseif command == "120103" then command_name = "CMD_SET_STEP_GOAL"
    elseif command == "120104" then command_name = "CMD_SET_USER_DATA"
    elseif command == "120105" then command_name = "CMD_SET_LONG_SIT_REMINDER"
    elseif command == "120106" then command_name = "CMD_SET_ARM"
    elseif command == "120107" then command_name = "CMD_NOTIFICATIONS_ENABLE"
    elseif command == "120108" then command_name = "CMD_SET_DEVICE_REMINDERS"
    elseif command == "120109" then command_name = "CMD_SET_DISPLAY_ON_LIFT"
    elseif command == "12010a" then command_name = "GET_pairing"
    elseif command == "12010b" then command_name = "FIND_BAND"
    elseif command == "12010c" then command_name = "SET_camera"
    elseif command == "12010d" then command_name = "Receive_HR?"
    elseif command == "12010e" then command_name = "Receive_SPo2?"
    elseif command == "12010f" then command_name = "CMD_SET_SLEEP_TIMES"
    elseif command == "120111" then command_name = "CMD_NOTIFICATION_CALL"
    elseif command == "120112" then command_name = "CMD_NOTIFICATION_MESSAGE"
    elseif command == "120114" then command_name = "CMD_DND"
    elseif command == "120115" then command_name = "CMD_SET_LANGUAGE"
    elseif command == "120118" then command_name = "GET_HR_measurement"
    elseif command == "120119" then command_name = "OTA_update"
    elseif command == "120120" then command_name = "CMD_WEATHER"
    elseif command == "1201ff" then command_name = "GET_phone_type"
    elseif command == "150106" then command_name = "SET realtime steps"
    elseif command == "15010c" then command_name = "Receiving_sports_data"
    elseif command == "15010d" then command_name = "Fetch_sports_data"
    elseif command == "15010e" then command_name = "Receiving_HR_data"
    elseif command == "150102" then command_name = "Receiving_steps_data"
    elseif command == "150103" then command_name = "Receiving_sleep_data"
    elseif command == "1a0101" then command_name = "GET_personal_info"
    elseif command == "1a0102" then command_name = "GET steps goal"
    elseif command == "1a0108" then command_name = "GET auto HR"
    elseif command == "1a010a" then command_name = "GET BT address"
    elseif command == "1a010b" then command_name = "Sending some test instruction"
    elseif command == "1a010c" then command_name = "GET BT name"
    elseif command == "1a010d" then command_name = "GET contacts"
    elseif command == "1a010f" then command_name = "GET temperature unit"
    elseif command == "1a0110" then command_name = "GET_hardware_info"
    elseif command == "1d0101" then command_name = "CMD_RESET"
    elseif command == "200102" then command_name = "GET_band_info"
    end

    return command_name
end

function process_long_sit(buffer, subtree,pinfo)

    local display_enabled = "Disabling"
    local enabled = tostring(buffer(9,1))
    if enabled == "01" then 
        display_enabled = "Enabling"
    end
    code=tostring(buffer(12,1))
    from=tostring(buffer(13,1))
    to=  tostring(buffer(14,1))

    function code_to_minutes(input)
        --print(input)
        result="Unknown"
        if input=="03" then result = 45
        elseif input=="04" then result = 60
        elseif input=="05" then result = 75
        elseif input=="06" then result = 90
        elseif input=="07" then result = 105
        elseif input=="08" then result = 120		
        end
        return result .. " minutes"
    end

    print("from " .. from)
    print("to " .. to)
    print("code " .. code)
    print(flags_enabled)
    print(code_to_minutes(code))
    print(tonumber(from,16))
    print(tonumber(to,16))

    subtree:add(long_sit_enabled, buffer(9,1)):append_text(" (" .. display_enabled .. ")")
    subtree:add(long_sit_code, buffer(12,1)):append_text(" (" .. code_to_minutes(code) .. ")")
    subtree:add(long_sit_from, buffer(13,1)):append_text(" (" .. tonumber(from,16) .. ")")
    subtree:add(long_sit_to ,buffer(14,1)):append_text(" (" .. tonumber(to,16) .. ")")

    pinfo.cols.info=display_enabled .. " long sit reminder"
end

function command_to_dec(command)
    local first = command:sub(1,2)
    local second = command:sub(5,6)
    return tonumber(first,16) .. "/" .. tonumber(second,16)
end

function process_display_lift(buffer, subtree,pinfo)
    local display_enabled = "Disabling"
    local enabled = tostring(buffer(8,1))
    if enabled == "01" then 
        display_enabled = "Enabling"
        from=tostring(buffer(9,2))
        to=  tostring(buffer(11,2))
        print("from " .. from)
        print("to " .. to)
        print(flags_enabled)
        print(tonumber(from,16))
        print(tonumber(to,16))
    end


    subtree:add(display_ol_enabled, buffer(8,1)):append_text(" (" .. display_enabled .. ")")
    if enabled == "01" then
        subtree:add(display_ol_from, buffer(9,2)):append_text(" (" .. tonumber(from,16)/60 .. ")")
        subtree:add(display_ol_to ,buffer(11,2)):append_text(" (" .. tonumber(to,16)/60 .. ")")
    end
    pinfo.cols.info=display_enabled .. " display on lift"
end

function process_sports_data(buffer, subtree,pinfo)
    length = buffer:len()
    if length < 12 then return end
    local subbuf = buffer(10)
    print("day data: " .. subbuf)
    steps=tostring(subbuf(0,4))
    dist=  tostring(subbuf(4,4))
    cal=  tostring(subbuf(8,2))
    print("cal: " .. cal)

    subtree:add(sports_data, buffer(10)):append_text(" (dist: " .. tonumber(dist,16)/1000 .. " cal: ".. tonumber(cal,16) .. " steps: " .. tonumber(steps,16) .. ")")
end

function process_sleep_data(buffer, subtree,pinfo)
    length = buffer:len()
    if length < 10 then return end
    local subbuf = buffer(8)
    print(subbuf)
    data = subbuf(0,2):uint()
    ts=get_date(data)

    subtree:add(data, buffer(8,4)):append_text(" Date: " .. os.date('%d:%m:%Y', ts))
    for i = 4, length-8-4, 4
       do
         subdata = subbuf(i,4)
         
         subtree:add(data, subdata):append_text(" Subdata: " .. subdata)
        end
end
function process_step_data(buffer, subtree,pinfo)
    length = buffer:len()
    if length < 10 then return end
    local subbuf = buffer(8)
    print(subbuf)
    data = subbuf(0,2):uint()
    ts=get_date(data)
    subtree:add(data, buffer(8,4)):append_text(" Date: " .. os.date('%d:%m:%Y', ts) .. " Subdata length: " .. length)

    for i = 4, length-8-4, 8
       do
         subdata = subbuf(i,8)
         print("subdata: " .. subdata .. " : " .. tonumber(tostring(subdata),16))
         time = get_time(subdata(0,2))
         steps = 0
         steps=bit.rshift(tonumber(tostring(subdata),16),52)
         --activetime=bit.band(bit.rshift(tonumber(tostring(subdata),16),48),0)
         activetime=bit.rshift(tonumber(tostring(subdata),16),48)
         calories=tostring(bit.band(tonumber(tostring(subdata),16),0x7ffff))
         
         subtree:add(data, subdata):append_text(" Subdata: " .. subdata .. " Time: " .. time .. " Steps: " .. steps .. " Active time: " .. activetime .. " Calories: " .. calories)
        end
end


function get_time(data)
    return data
end


function get_date(data)
    local year = tostring(bit.rshift(data,9)+2000)
    local month = tostring(bit.band(bit.rshift(data,5),0xf))
    local day = tostring(bit.band(data,0x1f))

    local dt = {year=year, month=month, day=day, hour=0, min=0, sec=0}
    local ts=os.time(dt)
    return ts
end

function process_heartrate_data(buffer, subtree,pinfo)
    length = buffer:len()
    if length < 10 then return end
    local subbuf = buffer(8)
    print(subbuf)

    local hr=tostring(subbuf(11,1):uint())
    local low=tostring(subbuf(10,1):uint())
    local high=tostring(subbuf(9,1):uint())
    local spo=tostring(subbuf(8,1):uint())
    local time=subbuf(6,2):uint()

    local dt = {year=1, month=1, day=1, hour=0, min=0, sec=0}
    local ts=os.time(dt)
    ts = ts + time

    subtree:add(heartrate_data, buffer(8)):append_text(" (Time: " .. os.date('%H:%M:%S', ts) .. " Seconds: " .. time .. " HR: " .. hr .. " Low: ".. low .. " High: " .. high .. " SpO2: " .. spo .. ")")
end


function process_user_data(buffer, subtree,pinfo)
    length = buffer:len()
    if length < 10 then return end
    local subbuf = buffer(8)
    print("user data: " .. subbuf)
    local data = buffer(8):uint()
    print("user data2: " .. data)
    
    
    local gender = tostring(bit.band(bit.rshift(data,31),0xff))
    local age = tostring(bit.band(bit.rshift(data,24),0xff))
    local height = tostring(bit.band(bit.rshift(data,15),0xff))
    local weight = tostring(bit.band(bit.rshift(data,5),0xff))
    local distanceUnit = tostring(bit.band(bit.rshift(data,0),0xf))
    
    subtree:add(data):append_text("Gender: " .. gender  .. " age: " .. age .. " height: " .. height .. " weight: " .. weight  .. " distanceUnit: " .. distanceUnit)

end
function process_band_info(buffer, subtree,pinfo)
    length = buffer:len()
    if length < 10 then return end
    local subbuf = buffer(8)
    print(subbuf)

    local start = 6

    local data_len=subbuf(start,1):uint() 
    local info1 = subbuf(start+1,data_len):string()
    print(start .. " " .. data_len)
    start = start + data_len + 1

    local data_len=subbuf(start,1):uint() 
    local info2 = subbuf(start+1,data_len):string()
    print(start .. " " .. data_len)
    start = start + data_len + 1

    subtree:add(hardware_data):append_text("Info1: " .. info1 .. ", Info2: " .. info2)
end
function process_hardware_info(buffer, subtree,pinfo)
    length = buffer:len()
    if length < 10 then return end
    local subbuf = buffer(8)
    print(subbuf)

    local start = 0

    local data_len=subbuf(start,1):uint() 
    local info1 = subbuf(start+1,data_len):string()
    print(start .. " " .. data_len)
    start = start + data_len + 1

    local data_len=subbuf(start,1):uint() 
    local info2 = subbuf(start+1,data_len):string()
    print(start .. " " .. data_len)
    start = start + data_len + 1

    local info3=""
    data_len=subbuf(start,1):uint()
    print(start .. " " .. data_len)
    info3 = subbuf(start+1,data_len):string()
    subtree:add(hardware_data):append_text("LED: " .. info1 .. ", Gsensor: " .. info2 .. ", HR:" .. info3)
end

function process_step_goal(buffer, subtree,pinfo)
    length = buffer:len()
    if length < 4 then return end
    local subbuf = buffer(8)
    print(subbuf)

    subtree:add(step_goal, buffer(8)):append_text(" (step goal: " .. subbuf:uint() .. ")")
end
function fitpro_protocol.dissector(buffer, pinfo, tree)
    length = buffer:len()
    print(length)
    if length < 6 then return end

    pinfo.cols.protocol = fitpro_protocol.name

    local subtree = tree:add(fitpro_protocol, buffer(), "FitPro Protocol Data")

    local command = tostring(buffer(3,3))
    local command_name = get_command_name(command)
    local command_dec = command_to_dec(command)
    
    if command_name ~= "Unknown" then pinfo.cols.info=command_name end

    subtree:add(message_length, buffer(2,1))
    subtree:add(message_header, buffer(0,1))
    subtree:add(message_header2, buffer(0,1))
    subtree:add(message_command, buffer(3,3)):append_text(" (" .. command_dec .. " " .. command_name ..  ")")
    subtree:add(message_payload_size, buffer(7,1))
    subtree:add(message_payload_data, buffer(8))


    if command_name == "CMD_SET_LONG_SIT_REMINDER" then process_long_sit(buffer,subtree,pinfo) end
    if command_name == "CMD_SET_DISPLAY_ON_LIFT" then process_display_lift(buffer,subtree,pinfo) end
    if command_name == "Receiving_sports_data" then process_sports_data(buffer,subtree,pinfo) end
    if command_name == "CMD_SET_USER_DATA" then process_user_data(buffer,subtree,pinfo) end
--    if command_name == "GET_personal_info" then process_user_data(buffer,subtree,pinfo) end
    if command_name == "CMD_SET_STEP_GOAL" then process_step_goal(buffer,subtree,pinfo) end
    if command_name == "GET_hardware_info" then process_hardware_info(buffer,subtree,pinfo) end
    if command_name == "GET_band_info" then process_band_info(buffer,subtree,pinfo) end
    if command_name == "Receiving_HR_data" then process_heartrate_data(buffer,subtree,pinfo) end
    if command_name == "Receiving_steps_data" then process_step_data(buffer,subtree,pinfo) end
    if command_name == "Receiving_sleep_data" then process_sleep_data(buffer,subtree,pinfo) end
    --if command_name == "GET_band_info" then process_band_info(buffer,subtree,pinfo) end
    --if command_name == "GET_band_info" then process_band_info(buffer,subtree,pinfo) end
    --if command_name == "GET_band_info" then process_band_info(buffer,subtree,pinfo) end
    --if command_name == "GET_band_info" then process_band_info(buffer,subtree,pinfo) end

end



-- register the protocol based on the btatt handle, 
local btatt_handle = DissectorTable.get("btatt.handle")
btatt_handle:add(0x0033, fitpro_protocol) --Sent
btatt_handle:add(0x0030, fitpro_protocol) --Received
