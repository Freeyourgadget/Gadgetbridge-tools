withings_protocol = Proto("Withings", "Withings Smart Watch Protocol")

unk01 = ProtoField.uint8("withings.unk01", "Unknown", base.HEX)
opcode = ProtoField.uint16("withings.opcode", "Opcode", base.HEX_DEC)
length = ProtoField.uint16("withings.length", "Length", base.DEC)

field_code = ProtoField.uint16("withings.field_code", "Field", base.HEX_DEC)
field_length = ProtoField.uint16("withings.field_length", "Length", base.DEC)
field_data = ProtoField.bytes("withings.field_data", "Value", base.NONE)

withings_protocol.fields = {
  unk01, opcode, length, -- header
  field_code, field_length, field_data, -- data (TLV)
}

function withings_protocol.dissector(buffer, pinfo, tree)
  -- message header
  buf_len = buffer:len()
  if buf_len < 5 then return end
  
  pinfo.cols.protocol = withings_protocol.name
  
  local subtree = tree:add(withings_protocol, buffer(), "Withings Message")
  
  local opcode_number = buffer(1,2):int()
  local opcode_name = get_opcode_name(opcode_number)
  subtree:add(opcode, buffer(1,2)):append_text(" (" .. opcode_name .. ")")
  
  local data_len = buffer(3,2):uint()
  subtree:add(length, buffer(3,2))
  
  -- message data
  local offset = 5
  while offset < data_len + 5 do
    -- field header
    local struct_code = buffer(offset+0,2):int()
    local struct_name = get_struct_name(struct_code)

    local struct_len = buffer(offset+2,2):int()

    local fieldtree = subtree:add(withings_protocol, buffer(offset,struct_len+4), "Struct"):append_text(": " .. struct_name .. " (" .. struct_code .. ")")
    fieldtree:add(field_code, buffer(offset+0,2)):append_text(" (" .. struct_name .. ")")
    fieldtree:add(field_length, buffer(offset+2,2))

    -- field data
    if struct_len > 0 then
      fieldtree:add(field_data, buffer(offset+4,struct_len))
    end

    offset = offset + struct_len + 4
  end
end

function get_opcode_name(opcode)
  local opcode_name = "Unknown"
  
      if opcode == 257 then opcode_name = "PROBE"
  elseif opcode == 273 then opcode_name = "INITIAL_CONNECT"
  elseif opcode == 274 then opcode_name = "SET_USER_UNIT"
  elseif opcode == 275 then opcode_name = "SETUP_FINISHED"
  elseif opcode == 277 then opcode_name = "SYNC_OK"
  elseif opcode == 282 then opcode_name = "SET_LOCALE"
  elseif opcode == 284 then opcode_name = "MOVE_HAND"
  elseif opcode == 286 then opcode_name = "START_HANDS_CALIBRATION"
  elseif opcode == 287 then opcode_name = "STOP_HANDS_CALIBRATION"
  elseif opcode == 293 then opcode_name = "GET_ALARM"
  elseif opcode == 296 then opcode_name = "CHALLENGE"
  elseif opcode == 298 then opcode_name = "GET_ALARM_SETTINGS"
  elseif opcode == 315 then opcode_name = "GET_WORKOUT_SCREEN_LIST"
  elseif opcode == 316 then opcode_name = "SET_WORKOUT_SCREEN"
  elseif opcode == 317 then opcode_name = "START_LIVE_WORKOUT"
  elseif opcode == 318 then opcode_name = "STOP_LIVE_WORKOUT"
  elseif opcode == 320 then opcode_name = "LIVE_WORKOUT_DATA"
  elseif opcode == 321 then opcode_name = "SYNC"
  elseif opcode == 325 then opcode_name = "SET_ALARM"
  elseif opcode == 1281 then opcode_name = "SET_TIME"
  elseif opcode == 1282 then opcode_name = "SET_USER"
  elseif opcode == 1283 then opcode_name = "GET_USER"
  elseif opcode == 1284 then opcode_name = "GET_BATTERY_STATUS"
  elseif opcode == 1286 then opcode_name = "GET_MOVEMENT_SAMPLES"
  elseif opcode == 1290 then opcode_name = "SET_ACTIVITY_TARGET"
  elseif opcode == 1292 then opcode_name = "SET_SCREEN_LIST"
  elseif opcode == 1293 then opcode_name = "GET_SCREEN_SETTINGS"
  elseif opcode == 2330 then opcode_name = "GET_ALARM_ENABLED"
  elseif opcode == 2331 then opcode_name = "SET_ALARM_ENABLED"
  elseif opcode == 2343 then opcode_name = "GET_HR"
  elseif opcode == 2344 then opcode_name = "GET_HEARTRATE_SAMPLES"
  elseif opcode == 2345 then opcode_name = "SET_ANCS_STATUS"
  elseif opcode == 2353 then opcode_name = "GET_ANCS_STATUS"
  elseif opcode == 2371 then opcode_name = "GET_SPORT_MODE"
  elseif opcode == 2403 then opcode_name = "GET_UNICODE_GLYPH"
  elseif opcode == 2404 then opcode_name = "GET_NOTIFICATION"
  elseif opcode == 2424 then opcode_name = "GET_ACTIVITY_SAMPLES"
  elseif opcode == 16705 then opcode_name = "SYNC_RESPONSE"
  end
  
  return opcode_name
end

function get_struct_name(code)
  local struct_name = "Unknown"
  
      if code == 256 then struct_name = "END_OF_TRANSMISSION"
  elseif code == 257 then struct_name = "PROBE_REPLY"
  elseif code == 281 then struct_name = "USER_UNIT"
  elseif code == 289 then struct_name = "LOCALE"
  elseif code == 290 then struct_name = "CHALLENGE"
  elseif code == 291 then struct_name = "CHALLENGE_RESPONSE"
  elseif code == 298 then struct_name = "PROBE"
  elseif code == 316 then struct_name = "WORKOUT_SCREEN_LIST"
  elseif code == 317 then struct_name = "WORKOUT_SCREEN_DATA"
  elseif code == 321 then struct_name = "WORKOUT_GPS_STATE"
  elseif code == 1281 then struct_name = "TIME"
  elseif code == 1283 then struct_name = "USER"
  elseif code == 1284 then struct_name = "BATTERY_STATUS"
  elseif code == 1286 then struct_name = "GET_ACTIVITY_SAMPLES"
  elseif code == 1292 then struct_name = "MOVE_HAND"
  elseif code == 1297 then struct_name = "ACTIVITY_TARGET"
  elseif code == 1298 then struct_name = "ALARM"
  elseif code == 1299 then struct_name = "USER_SECRET"
  elseif code == 1300 then struct_name = "ALARM_NAME"
  elseif code == 1302 then struct_name = "SCREEN_SETTINGS"
  elseif code == 1537 then struct_name = "ACTIVITY_SAMPLE_TIME"
  elseif code == 1538 then struct_name = "ACTIVITY_SAMPLE_DURATION"
  elseif code == 1539 then struct_name = "ACTIVITY_SAMPLE_MOVEMENT"
  elseif code == 1540 then struct_name = "ACTIVITY_SAMPLE_RUN"
  elseif code == 1541 then struct_name = "ACTIVITY_SAMPLE_WALK"
  elseif code == 1543 then struct_name = "ACTIVITY_SAMPLE_SLEEP"
  elseif code == 1544 then struct_name = "ACTIVITY_SAMPLE_CALORIES"
  elseif code == 1546 then struct_name = "ACTIVITY_SAMPLE_CALORIES_2"
  elseif code == 1546 then struct_name = "ACTIVITY_SAMPLE_UNKNOWN"
  elseif code == 1549 then struct_name = "ACTIVITY_SAMPLE_SWIM"
  elseif code == 2329 then struct_name = "ALARM_STATUS"
  elseif code == 2343 then struct_name = "HR"
  elseif code == 2344 then struct_name = "PROBE_OS_VERSION"
  elseif code == 2345 then struct_name = "ACTIVITY_HR"
  elseif code == 2346 then struct_name = "ANCS_STATUS"
  elseif code == 2369 then struct_name = "LIVE_HR"
  elseif code == 2390 then struct_name = "STEPS"
  elseif code == 2396 then struct_name = "GLYPH_ID"
  elseif code == 2397 then struct_name = "IMAGE_META_DATA"
  elseif code == 2398 then struct_name = "IMAGE_DATA"
  elseif code == 2404 then struct_name = "NOTIFICATION_APP_ID"
  elseif code == 2409 then struct_name = "WORKOUT_TYPE"
  elseif code == 2418 then struct_name = "LIVE_WORKOUT_START"
  elseif code == 2419 then struct_name = "LIVE_WORKOUT_END"
  elseif code == 2439 then struct_name = "LIVE_WORKOUT_PAUSE_STATE"
  end
  
  return struct_name
end

local btatt_handle = DissectorTable.get("btatt.handle")
btatt_handle:add(0x0016, withings_protocol)
