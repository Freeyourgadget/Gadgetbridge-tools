local gcrypt = require("luagcrypt")

local btatt_handle = Field.new("btatt.handle")
local btatt_opcode = Field.new("btatt.opcode")
local btatt_uuid = Field.new("btatt.uuid128")

cmf_proto = Proto("cmf_watch_pro_ble", "Nothing CMF Watch Pro BLE Protocol")

cmf_proto.prefs.k1 = Pref.string("K1 (hex)", "", "128-bit AES key, in hex")

cmf_proto.fields.chunk_cnt = ProtoField.uint8("cmf.chunk_cnt", "Chunk Count", base.HEX)
cmf_proto.fields.chunk_idx = ProtoField.uint8("cmf.chunk_idx", "Chunk Index", base.HEX)
cmf_proto.fields.session_key = ProtoField.bytes("cmf.session_key", "Session Key", base.COLON)
cmf_proto.fields.encrypted_payload = ProtoField.bytes("cmf.encrypted_payload", "Encrypted Payload", base.COLON)
cmf_proto.fields.payload = ProtoField.bytes("cmf.payload", "Payload", base.COLON)

function bytes_str_to_hex(bytes)
    hex = ''
    for j = 1, #bytes do
        local c = string.byte(bytes:sub(j, j))
        if hex:len() > 0 then
            hex = hex .. ':'
        end
        hex = hex .. string.format("%02x", c)
    end
    return hex
end

function hex_to_bytes_str(hex)
    -- as per https://github.com/Lekensteyn/luagcrypt/blob/master/luagcrypt_test.lua
    if string.match(hex, "[^0-9a-fA-F]") then
        error("Invalid characters in hex")
    end
    if string.len(hex) % 2 == 1 then
        error("Hex string length must be a multiple of two")
    end
    local s = string.gsub(hex, "..", function(v)
        return string.char(tonumber(v, 16))
    end)
    return s
end

GCRYPT_INITIALIZED = false
AES_SESSION_KEY = nil
AES_IV = hex_to_bytes_str("5051525354555657606162636465665a")

COMMAND_TYPES = {
    ["0x005c 0x0001"] = { "BATTERY", function (subtree, payload) end },
    ["0x005b 0x0001"] = { "FIND_PHONE", function (subtree, payload) end },
    ["0x005d 0x0001"] = { "FIND_WATCH", function (subtree, payload) end },
    ["0x005c 0x0002"] = { "TRIGGER_SYNC", function (subtree, payload) end },
    ["0x0060 0x0002"] = { "STANDING_REMINDER_GET", function (subtree, payload) end },
    ["0x0060 0x0001"] = { "STANDING_REMINDER_SET", function (subtree, payload) end },
    ["0x0061 0x0002"] = { "WATER_REMINDER_GET", function (subtree, payload) end },
    ["0x0061 0x0001"] = { "WATER_REMINDER_SET", function (subtree, payload) end },
    ["0x00d5 0x0002"] = { "CONTACTS_GET", function (subtree, payload) end },
    ["0x00d5 0x0001"] = { "CONTACTS_SET", function (subtree, payload) end },
    ["0x0063 0x0002"] = { "ALARMS_GET", function (subtree, payload) end },
    ["0x0063 0x0001"] = { "ALARMS_SET", function (subtree, payload) end },
    ["0xffff 0x8005"] = { "ACTIVITY_FETCH_1", function (subtree, payload) end },
    ["0xffff 0x9057"] = { "ACTIVITY_FETCH_2", function (subtree, payload) end },
    ["0xffff 0x0005"] = { "ACTIVITY_FETCH_ACK_1", function (subtree, payload) end },
    ["0xffff 0xa057"] = { "ACTIVITY_FETCH_ACK_2", function (subtree, payload) end },
    ["0x0056 0x0001"] = { "ACTIVITY_DATA", function (subtree, payload) end },
    ["0x0057 0x0001"] = { "WORKOUT_SUMMARY", function (subtree, payload) end },
    ["0xffff 0xa05a"] = { "WORKOUT_GPS", function (subtree, payload) end },
    ["0x00da 0x0001"] = { "HEART_RATE_RESTING", function (subtree, payload) end },
    ["0x0055 0x0001"] = { "SPO2", function (subtree, payload) end },
    ["0x009d 0x0001"] = { "STRESS", function (subtree, payload) end },
    ["0x0053 0x0001"] = { "HEART_RATE_MANUAL_AUTO", function (subtree, payload) end },
    ["0x0058 0x0001"] = { "SLEEP_DATA", function (subtree, payload) end },
    ["0x00e0 0x0001"] = { "HEART_RATE_WORKOUT", function (subtree, payload) end },
    ["0x0065 0x0001"] = { "APP_NOTIFICATION", function (subtree, payload) end },
    ["0xffff 0x905c"] = { "MUSIC_INFO_SET", function (subtree, payload) end },
    ["0xffff 0xa05c"] = { "MUSIC_INFO_ACK", function (subtree, payload) end },
    ["0xffff 0xa05d"] = { "MUSIC_BUTTON", function (subtree, payload) end },
    ["0x00dc 0x0001"] = { "SPORTS_SET", function (subtree, payload) end },
    ["0xffff 0x9058"] = { "LANGUAGE_SET", function (subtree, payload) end },
    ["0xffff 0xa06b"] = { "LANGUAGE_RET?", function (subtree, payload) end },
    ["0x009b 0x0002"] = { "HEART_MONITORING_ENABLED_GET", function (subtree, payload) end },
    ["0x009b 0x0001"] = { "HEART_MONITORING_ENABLED_SET", function (subtree, payload) end },
    ["0xffff 0x906b"] = { "WEATHER_SET_1", function (subtree, payload) end },
    ["0x0066 0x0001"] = { "WEATHER_SET_2", function (subtree, payload) end },
    ["0xffff 0x9066"] = { "CALL_REMINDER", function (subtree, payload) end },
    ["0xffff 0x9059"] = { "HEART_MONITORING_ALERTS", function (subtree, payload) end },
    ["0x005e 0x0001"] = { "GOALS_SET", function (subtree, payload) end },
    ["0xffff 0x8004"] = { "TIME", function (subtree, payload) end },
    ["0x005f 0x0001"] = { "TIME_FORMAT", function (subtree, payload) end },
    ["0xffff 0x9067"] = { "UNIT_LENGTH", function (subtree, payload) end },
    ["0xffff 0x9068"] = { "UNIT_TEMPERATURE", function (subtree, payload) end },
    ["0x009f 0x0001"] = { "WATCHFACE", function (subtree, payload) end },
    ["0x0062 0x0001"] = { "WAKE_ON_WRIST_RAISE", function (subtree, payload) end },
    ["0x0099 0x0001"] = { "DO_NOT_DISTURB", function (subtree, payload) end },
    [0x0003] = { "ACK", function (subtree, payload) end },
    ["0xffff 0x8047"] = { "AUTH_PAIR_REQUEST", function (subtree, payload) end },
    ["0xffff 0x0048"] = { "AUTH_PAIR_REPLY", function (subtree, payload) end },
    ["0xffff 0x905e"] = { "DATA_TRANSFER_AGPS_INIT_REQUEST", function (subtree, payload) end },
    ["0xffff 0xa05e"] = { "DATA_TRANSFER_AGPS_INIT_REPLY", function (subtree, payload) end },
    ["0xffff 0xa060"] = { "DATA_TRANSFER_AGPS_FINISH_ACK_1", function (subtree, payload) end },
    ["0xffff 0x9060"] = { "DATA_TRANSFER_AGPS_FINISH_ACK_2", function (subtree, payload) end },
    ["0xffff 0x9063"] = { "DATA_TRANSFER_WATCHFACE_INIT_REQUEST", function (subtree, payload) end },
    ["0xffff 0xa063"] = { "DATA_TRANSFER_WATCHFACE_INIT_REPLY", function (subtree, payload) end },
    ["0xffff 0xa065"] = { "DATA_TRANSFER_WATCHFACE_FINISH_ACK_1", function (subtree, payload) end },
    ["0xffff 0x9065"] = { "DATA_TRANSFER_WATCHFACE_FINISH_ACK_2", function (subtree, payload) end },
    ["0xffff 0x9064"] = { "DATA_CHUNK_WRITE_WATCHFACE", function (subtree, payload) end },
    ["0xffff 0xa064"] = { "DATA_CHUNK_REQUEST_WATCHFACE", function (subtree, payload) end },
    ["0xffff 0x905f"] = { "DATA_CHUNK_WRITE_AGPS", function (subtree, payload) end },
    ["0xffff 0xa05f"] = { "DATA_CHUNK_REQUEST_AGPS", function (subtree, payload) end },
    ["0xffff 0x906a"] = { "GPS_COORDS", function (subtree, payload) end },
    ["0x009a 0x0001"] = { "FACTORY_RESET", function (subtree, payload) end },
    ["0xffff 0x8006"] = { "FIRMWARE_VERSION_GET", function (subtree, payload) end },
    ["0xffff 0x0006"] = { "FIRMWARE_VERSION_RET", function (subtree, payload) end },
    ["0x00de 0x0002"] = { "SERIAL_NUMBER_GET", function (subtree, payload) end },
    ["0x00de 0x0001"] = { "SERIAL_NUMBER_RET", function (subtree, payload) end },
    ["0xffff 0x8049"] = { "AUTH_PHONE_NAME", function (subtree, payload) end },
    ["0xffff 0x0049"] = { "AUTH_WATCH_MAC", function (subtree, payload) end },
    ["0xffff 0x804b"] = { "AUTH_NONCE_REQUEST", function (subtree, payload) end },
    ["0xffff 0x004c"] = { "AUTH_NONCE_REPLY", function (subtree, payload)
        local md = gcrypt.Hash(gcrypt.MD_SHA256)

        md:write(hex_to_bytes_str(payload:bytes():tohex()))
        md:write(hex_to_bytes_str(cmf_proto.prefs.k1))
        local newSessionKey = md:read():sub(0, 16)

        subtree:add(payload, "New Session Key:", bytes_str_to_hex(newSessionKey))
        AES_SESSION_KEY = newSessionKey
    end },

    ["0xffff 0x804d"] = { "AUTHENTICATED_CONFIRM_REQUEST", function (subtree, payload) end },
    ["0xffff 0x0004"] = { "AUTHENTICATED_CONFIRM_REPLY", function (subtree, payload) end },
}

local function parse_payload(proto, payload, pinfo, subtree, cmd1, cmd2)
    local payloadWithoutCrc = payload

    local commandTypesKey = string.format("0x%04x 0x%04x", cmd1, cmd2)

    if cmd2 == 0x0003 then
        pinfo.cols.info:append(" | ACK")
        return
    end

    if COMMAND_TYPES[commandTypesKey] ~= nil then
        pinfo.cols.info:append(" | " .. COMMAND_TYPES[commandTypesKey][1])
        --pinfo.cols.info = "CMF - " .. COMMAND_TYPES[cmd][1]
    else
        utils.warn(subtree, string.format("Unknown command cmd1=0x%04X cmd2=0x%04X", cmd1, cmd2))
        return
    end

    if COMMAND_TYPES[commandTypesKey][2] ~= nil then
        COMMAND_TYPES[commandTypesKey][2](subtree, payloadWithoutCrc)
    else
        utils.warn(subtree, string.format("No parser for cmd1=0x%04X cmd2=0x%04X", cmd1, cmd2))
        return
    end

end

local function parse_command(proto, payload, pinfo, subtree)
    local cmd1 = payload(3, 2):uint()
    local cmd2 = payload(9, 2):uint()
    subtree:add(payload(1, 2), string.format("Encrypted Payload length: %d", payload(1, 2):uint()))
    subtree:add(payload(3, 2), string.format("cmd1: 0x%04x", cmd1))
    --subtree:add(payload(5, 2), string.format("Chunk count: 0x%04x", payload(5, 2):uint()))
    subtree:add(proto.fields.chunk_cnt, payload(5, 2))
    --subtree:add(payload(7, 2), string.format("Chunk index: 0x%04x", payload(7, 2):uint())) -- starts at 1
    subtree:add(proto.fields.chunk_idx, payload(7, 2))
    subtree:add(payload(9, 2), string.format("cmd2: 0x%04x", cmd2))

    pinfo.cols.info:append(string.format(
        " cmd1=%04x cmd2=%04x",
        cmd1,
        cmd2
    ))

    if payload(1, 2):uint() > 0 then
        local encryptedPayload = payload(11, payload(1, 2):uint())
        subtree:add(proto.fields.encrypted_payload, encryptedPayload)

        if gcrypt == nil then
            utils.error(subtree, "lugcrypt not installed, unable to decrypt")
            return
        end
        if not GCRYPT_INITIALIZED then
            GCRYPT_INITIALIZED = true
            gcrypt.init()
        end

        local payloadKey = hex_to_bytes_str(proto.prefs.k1)
        local shouldDecrypt = true

        local commandTypesKey = string.format("0x%04x 0x%04x", cmd1, cmd2)
        if AES_SESSION_KEY ~= nil and (COMMAND_TYPES[commandTypesKey] == nil or string.sub(COMMAND_TYPES[commandTypesKey][1],1,string.len("AUTH_")) ~= "AUTH_") then
            payloadKey = AES_SESSION_KEY
        end
        shouldDecrypt = COMMAND_TYPES[commandTypesKey] == nil or (
            string.sub(COMMAND_TYPES[commandTypesKey][1],1,string.len("DATA_CHUNK_WRITE_")) ~= "DATA_CHUNK_WRITE_"
            and string.sub(COMMAND_TYPES[commandTypesKey][1],1,string.len("AUTH_PAIR_")) ~= "AUTH_PAIR_"
        )

        if shouldDecrypt then
            local cipher = gcrypt.Cipher(gcrypt.CIPHER_AES128, gcrypt.CIPHER_MODE_CBC)
            cipher:setkey(payloadKey)
            cipher:setiv(AES_IV)

            subtree:add(encryptedPayload, "Session Key:", bytes_str_to_hex(payloadKey))

            local decryptedByteString = cipher:decrypt(hex_to_bytes_str(encryptedPayload:bytes():tohex()))
            local decryptedBytes = ByteArray.new(bytes_str_to_hex(decryptedByteString):gsub(":", ""))
            local paddingLen = decryptedBytes(decryptedBytes:len() - 1, 1):uint()
            local decryptedTvb = ByteArray.tvb(decryptedBytes(0, decryptedBytes:len() - paddingLen), "Decrypted Payload")
            local decryptedTvbWithoutCrc = ByteArray.tvb(decryptedBytes(0, decryptedBytes:len() - paddingLen - 4), "Decrypted Payload without CRC")

            local subtreePayload = subtree:add(proto.fields.payload, decryptedTvbWithoutCrc(0, decryptedTvbWithoutCrc:len()))

            local crc32 = decryptedTvb(decryptedTvb:len() - 4, 4):le_uint()
            subtreePayload:add(decryptedTvb(decryptedTvb:len() - 4, 4), string.format("CRC32: 0x%08x", crc32))

            parse_payload(proto, decryptedTvbWithoutCrc, pinfo, subtreePayload, cmd1, cmd2)
        else
            local subtreePayload = subtree:add(proto.fields.payload, encryptedPayload(0, encryptedPayload:len()))

            parse_payload(proto, encryptedPayload, pinfo, subtreePayload, cmd1, cmd2)
        end
    else
        local emptyBytes = ByteArray.new(bytes_str_to_hex(""))
        local emptyTvb = ByteArray.tvb(emptyBytes, "Empty Payload")
        local subtreePayload = subtree:add(proto.fields.payload, emptyTvb(0, 0))
        parse_payload(proto, emptyTvb, pinfo, subtreePayload, cmd1, cmd2)
    end
end

BTATT_OPCODES = {
    [0x0a] = "Read Request",
    [0x0b] = "Read Response",
    [0x12] = "Write Request",
    [0x13] = "Write Response",
    [0x1d] = "Value Indication",
    [0x1b] = "Handle Value Notification",
    [0x52] = "Write Command",
}

CMF_HANDLES = {
    [0x0038] = { "CHARACTERISTIC_CMF_WRITE >", parse_command },
    [0x003a] = { "CHARACTERISTIC_CMF_READ  <", parse_command },
    [0x0044] = { "CHARACTERISTIC_CMF_DATA_WRITE >", parse_command },
    [0x0046] = { "CHARACTERISTIC_CMF_DATA_READ  <", parse_command },
}

function is_valid_btatt_opcode()
    return BTATT_OPCODES[btatt_opcode().value] ~= nil
end

cmf_proto.dissector = function(buffer, pinfo, tree)
    if not is_valid_btatt_opcode() then
        return
    end

    pinfo.cols.info = CMF_HANDLES[btatt_handle().value][1]
    if CMF_HANDLES[btatt_handle().value][2] ~= nil then
        pinfo.cols.protocol = cmf_proto.name

        local subtree_data = tree:add(cmf_proto, buffer, "")
        CMF_HANDLES[btatt_handle().value][2](cmf_proto, buffer, pinfo, subtree_data)
    end
end

local btatt_handle_table = DissectorTable.get("btatt.handle")
for handle, _ in pairs(CMF_HANDLES) do
    btatt_handle_table:add(handle, cmf_proto)
end
